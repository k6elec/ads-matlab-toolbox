function [spec,rawSimData,superRawSimData] = ADSsimulateMS(NetlistFileName,MSdef,varargin)
% ADSSIMULATEMS calculates the response of the system to the multisine. Voltages and currents at the stage interfaces are saved.
%
%    [spec,rawSimData,superRawSimData] =  ADSSIMULATEMS(NetlistFileName,MSdef)
%    [spec,rawSimData,superRawSimData] =  ADSSIMULATEMS(NetlistFileName,MSdef,'ParamName',ParamValue)
%
% In the following, we use F to indicate the amount of tones
% in the multisine, M for the amount of realisations to be simulated and S
% for the amount of multisine sources in each realisation.
% Note: only the first multisine is used to calculate the simulation parameters for now. 
% The function works best when the frequency grids of the multisines are the same.
%
% Required Inputs:
%   NetlistFileName  Default:  CheckFunction: @isNetlist
%     the name of the netlist to be used in the simulation. Make sure the
%     netlist doesn't contain any simulation statements and that the node where
%     the multisine source has to be connected is floating.
%     You can also pass a cell array of strings that contains the netlist. that
%     way you don't have to write it into a temporary file.
%   MSdef  Default:  CheckFunction: @checkMSdef
%     Struct or vector of structs with length S which contains the information about the
%     multisine(s). Each struct needs at least the fields f0 grid MSnode ampl
%     and Rout, which describe the multisine, its output impedance and the node
%     it has to be connected to.
% Parameter-Value pairs:
%   simulator  Default: 'TRAN' CheckFunction: @(x)any(strcmpi(x,{'HB','TRAN','ENV'}))
%     simulator used. either 'HB','TRAN' or 'ENV'
%   numberOfRealisations  Default: 7 CheckFunction: @(x) isnumeric(x) && isscalar(x)
%     number of multisine realisations that has to be simulated. If the phase
%     is not passed to the function, each multisine gets a random phase.
%   oversample  Default: 10 CheckFunction: @(x) isnumeric(x) && isscalar(x)
%     Oversample ratio used to calculate the maximum time step in the TRAN
%     simulation or the order in the HB simulation.
%   periods  Default: 1.5 CheckFunction: @(x) isnumeric(x) && isscalar(x)
%     number of periods simulated in every TRAN simulation. All periods are
%     passed to the output, so it's best to use only the last one, because it
%     will contain the best steady-state response.
%   phase  Default: [] CheckFunction: @(x) isnumeric(x)|iscell(x)
%     phase matrix of the multisines used. This should be an FxMxS matrix or a
%     cell array of length S, filled with FxM matrices that contain the phase
%     for that particular multisine source. Use this parameter to control the
%     phase of the multisine. If you don't pass it, the multisine will get a
%     random phase.
%   CF  Default: Inf CheckFunction: @isscalar
%     Maximum crest factor of the multisine. The function generates a random
%     phase and just retries when the crest factor is too high, so you cannot
%     do fancy CF optimisation with this.
%   amplitude  Default: [] CheckFunction: @(x) iscell(x)
%     amplitude matrix of the multisines. This can be used in a similar way as
%     the phase matrix, to control the amplitude over the different
%     realisations of the multisine. ampl should be a cell array filled with
%     FxM matrices. The length of the cell array should be equal to the amount
%     of multisine sources present in the circuit.
%   DC  Default: [] CheckFunction: @(x) iscell(x)
%     DC-offset matrix of the multisines. This can be used in a similar way as
%     the phase matrix, to control the DC-offset over the different
%     realisations of the multisine. DC should be a cell array filled with
%     1xM matrices. The length of the cell array should be equal to the amount
%     of multisine sources present in the circuit.
%   freqWarping  Default: false CheckFunction: @islogical
%     boolean which indicates whether or not to compensate the frequency
%     warping after a TRAN simulation & ENV simulation that is using
%     the trapezoidal integration method.
%   HB_freq  Default: [] CheckFunction: @isvector
%     Fundamental frequency components wanted in the HB simulation. For a
%     lowpass multisine, this is normally just the base frequency f0. For a
%     bandpass multisine, you should pass the carrier frequency fc and the base
%     frequency f0
%   HB_order  Default: [] CheckFunction: @isvector
%     Wanted order vector for the HB simulation. The default uses a certain
%     degree of nonlinearity O and the amount of tones in the multisine T to
%     calculate the order automatically. For a lowpass multisine the order will
%     just be T*O. For a bandpass multisine, the function uses O for the fc
%     component and O*(T-1)/2+O-1 for the f0 component, which takes the diamond
%     truncation into account. HB_Order should have the same length as HB_freq.
%   OutFileName  Default: '' CheckFunction: @ischar
%     If you set the outFileName, ADS will create several files that contain
%     the steady-state final result of the simulation.
%   InFileName  Default: '' CheckFunction: @ischar
%     Filename of the file that contains the initial guess used in the harmonic balance
%     simulator. If several realisations are used, a number is added to the
%     filename.
%   CleanupFiles  Default: true CheckFunction: @islogical
%     CleanupFiles indicates to clean up the temp files or not. Usefull for
%     debugging.
%   saveDatasets  Default: false CheckFunction: @islogical
%     boolean to indicate whether the dataset of the simulation should be
%     saved. The datasets are copied to your working folder. You can open the
%     generated datasets with the dds tool of ADS (C:\ADS2014_01\bin\dds.exe)
%   SimSettingsStruct  Default: [] CheckFunction: @isstruct
%     SimSettingsStruct gives you direct access to the simulator settings by
%     passing a struct with the parameters. You can find out which parameters
%     to pass by looking at the help of ADSaddTRAN or ADSaddHB. Use this if you
%     want full control over the simulation, at your own risk off course :)
%   OptionSettingsStruct  Default: [] CheckFunction: @isstruct
%     OptionSettingsStruct gives you direct access to the options components
%     that's added to the netlist.
%   customFilter  Default: '' CheckFunction: @ischar
%     customFilter that is used in the MatlabOutput block. Use this if you want
%     to reduce the amount of data that is written to the .mat file
%   EnvOversample  Default: 20 CheckFunction: @isscalar
%     Defines the oversample used in an envelope simulation. This oversample
%     determines the sample time of the envelope simulation
%   EnvBandwidth  Default: 0.25 CheckFunction: @isscalar
%     the relative bandwidth used in the envelope simulation
%   EnvIntegOrder  Default: 1 CheckFunction: @isscalar
%     Integration order used in the envelope simulation. If EnvIntegOrder is
%     set to 1, backward euler is used. If EnvIntegOrder is set to 2 and
%     EnvUseGear is 0, then trapezoidal is used.
%   EnvUseGear  Default: 0 CheckFunction: @isscalar
%     If EnvUseGear is set to 1 and EnvIntegOrder is set to 2, then Gear's of
%     order two is used in the envelope simulation
%   EnvDomain  Default: 'freq' CheckFunction: @(x) any(strcmpi(x,{'freq','time'}))
%     domain in which the envelope simulation is returned. If 'time' domain is
%     selected, the result will be a struct with MxPxTxF matrices where T is
%     the amount of time samples and F will be the amount of tones in the
%     underlying HB simulation. There will be a 'freq' field of length F and a
%     'time' field of length T. If the 'freq' domain is chosen, the fft will be
%     applied to the envelope simulation result and the spectrum is rebuilt
%     around each carrier. The result is an MxPxF matrix
%   outputNestLevel  Default: 0 CheckFunction: @isscalar
%     Nest level of the voltage and currents that are sent to the output. Set
%     this to higher values if you want voltages and currents from sub-circuits
%     to appear in the results
%   onlyRaw  Default: 0 CheckFunction: @islogical
%     Can be used to give back only the raw simulation data. The first output
%     argument is just an ampty matrix then. This option is usefull if you are
%     working with a variable timestep in transient simulations. There the
%     routines that calculate the spectrum can fail because the amount of time
%     points is not fixed.
%   sourceFileName  Default: 'MsSource' CheckFunction: @ischar
%     filename of the multisine source used in the netlist
%   sourceName  Default: 'MsSource_inst' CheckFunction: @ischar
%     name of the source component in the netlist
%   sourceLocation  Default: [] CheckFunction: @ischar
%     folder where the source is saved
% 
% Outputs: 
%   spec Type: struct
%     Contains the response of the circuit to the multisine. The
%     struct contains several MxPxF matrices with the calculated spectra of
%     each named node or current in the circuit
%   rawSimData Type: cell array of structs
%     Contains the raw simulation data as loaded by the
%     ADSimportSimData function. It is a vector of length M
%   superRawSimData Type: cell array of structs
%     Contains the raw simulation data as it is put in the .mat file
%     by the matlaboutput function of ADS
%
% 
% See Also: ADSRUNSIMULATION, ADSIMPORTSIMDATA, ADSSIMULATEMSWAVES


%% parse the input

p = inputParserEgon();
p.FunctionName = 'ADSsimulateMS';

% the name of the netlist to be used in the simulation. Make sure the
% netlist doesn't contain any simulation statements and that the node where
% the multisine source has to be connected is floating.
% You can also pass a cell array of strings that contains the netlist. that
% way you don't have to write it into a temporary file.
p.addRequired('NetlistFileName',@isNetlist);

% Struct or vector of structs with length S which contains the information about the
% multisine(s). Each struct needs at least the fields f0 grid MSnode ampl
% and Rout, which describe the multisine, its output impedance and the node
% it has to be connected to.
p.addRequired('MSdef',@checkMSdef);

% simulator used. either 'HB','TRAN' or 'ENV'
p.addParamValue('simulator','TRAN',@(x)any(strcmpi(x,{'HB','TRAN','ENV'})));

% number of multisine realisations that has to be simulated. If the phase
% is not passed to the function, each multisine gets a random phase.
p.addParamValue('numberOfRealisations',7,@(x) isnumeric(x) && isscalar(x));

% Oversample ratio used to calculate the maximum time step in the TRAN
% simulation or the order in the HB simulation.
p.addParamValue('oversample',10,@(x) isnumeric(x) && isscalar(x));

% number of periods simulated in every TRAN simulation. All periods are
% passed to the output, so it's best to use only the last one, because it
% will contain the best steady-state response.
p.addParamValue('periods',1.5,@(x) isnumeric(x) && isscalar(x));

% phase matrix of the multisines used. This should be an FxMxS matrix or a
% cell array of length S, filled with FxM matrices that contain the phase
% for that particular multisine source. Use this parameter to control the
% phase of the multisine. If you don't pass it, the multisine will get a
% random phase.
p.addParamValue('phase',[],@(x) isnumeric(x)|iscell(x));

% Maximum crest factor of the multisine. The function generates a random
% phase and just retries when the crest factor is too high, so you cannot
% do fancy CF optimisation with this.
p.addParamValue('CF',Inf,@isscalar);

% amplitude matrix of the multisines. This can be used in a similar way as
% the phase matrix, to control the amplitude over the different
% realisations of the multisine. ampl should be a cell array filled with
% FxM matrices. The length of the cell array should be equal to the amount
% of multisine sources present in the circuit.
p.addParamValue('amplitude',[],@(x) iscell(x));

% DC-offset matrix of the multisines. This can be used in a similar way as
% the phase matrix, to control the DC-offset over the different
% realisations of the multisine. DC should be a cell array filled with
% 1xM matrices. The length of the cell array should be equal to the amount
% of multisine sources present in the circuit.
p.addParamValue('DC',[],@(x) iscell(x));

% boolean which indicates whether or not to compensate the frequency
% warping after a TRAN simulation & ENV simulation that is using
% the trapezoidal integration method.
p.addParamValue('freqWarping',false,@islogical);

% Fundamental frequency components wanted in the HB simulation. For a
% lowpass multisine, this is normally just the base frequency f0. For a
% bandpass multisine, you should pass the carrier frequency fc and the base
% frequency f0
p.addParamValue('HB_freq',[],@isvector);

% Wanted order vector for the HB simulation. The default uses a certain
% degree of nonlinearity O and the amount of tones in the multisine T to
% calculate the order automatically. For a lowpass multisine the order will
% just be T*O. For a bandpass multisine, the function uses O for the fc
% component and O*(T-1)/2+O-1 for the f0 component, which takes the diamond
% truncation into account. HB_Order should have the same length as HB_freq.
p.addParamValue('HB_order',[],@isvector);

% If you set the outFileName, ADS will create several files that contain
% the steady-state final result of the simulation.
p.addParamValue('OutFileName','',@ischar);

% Filename of the file that contains the initial guess used in the harmonic balance
% simulator. If several realisations are used, a number is added to the
% filename.
p.addParamValue('InFileName','',@ischar);

% CleanupFiles indicates to clean up the temp files or not. Usefull for
% debugging.
p.addParamValue('CleanupFiles',true,@islogical);

% boolean to indicate whether the dataset of the simulation should be
% saved. The datasets are copied to your working folder. You can open the
% generated datasets with the dds tool of ADS (C:\ADS2014_01\bin\dds.exe)
p.addParamValue('saveDatasets',false,@islogical);

% SimSettingsStruct gives you direct access to the simulator settings by
% passing a struct with the parameters. You can find out which parameters
% to pass by looking at the help of ADSaddTRAN or ADSaddHB. Use this if you
% want full control over the simulation, at your own risk off course :)
p.addParamValue('SimSettingsStruct',[],@isstruct);

% OptionSettingsStruct gives you direct access to the options components
% that's added to the netlist.
p.addParamValue('OptionSettingsStruct',[],@isstruct);

% customFilter that is used in the MatlabOutput block. Use this if you want
% to reduce the amount of data that is written to the .mat file
p.addParamValue('customFilter','',@ischar);

% Defines the oversample used in an envelope simulation. This oversample
% determines the sample time of the envelope simulation
p.addParamValue('EnvOversample',20,@isscalar);

% the relative bandwidth used in the envelope simulation
p.addParamValue('EnvBandwidth',0.25,@isscalar);

% Integration order used in the envelope simulation. If EnvIntegOrder is
% set to 1, backward euler is used. If EnvIntegOrder is set to 2 and
% EnvUseGear is 0, then trapezoidal is used.
p.addParamValue('EnvIntegOrder',1,@isscalar);

% If EnvUseGear is set to 1 and EnvIntegOrder is set to 2, then Gear's of
% order two is used in the envelope simulation
p.addParamValue('EnvUseGear',0,@isscalar);

% domain in which the envelope simulation is returned. If 'time' domain is
% selected, the result will be a struct with MxPxTxF matrices where T is
% the amount of time samples and F will be the amount of tones in the
% underlying HB simulation. There will be a 'freq' field of length F and a
% 'time' field of length T. If the 'freq' domain is chosen, the fft will be
% applied to the envelope simulation result and the spectrum is rebuilt
% around each carrier. The result is an MxPxF matrix
p.addParamValue('EnvDomain','freq',@(x) any(strcmpi(x,{'freq','time'})));

% Nest level of the voltage and currents that are sent to the output. Set
% this to higher values if you want voltages and currents from sub-circuits
% to appear in the results
p.addParamValue('outputNestLevel',0,@isscalar);

% Can be used to give back only the raw simulation data. The first output
% argument is just an ampty matrix then. This option is usefull if you are
% working with a variable timestep in transient simulations. There the
% routines that calculate the spectrum can fail because the amount of time
% points is not fixed.
p.addParamValue('onlyRaw',0,@islogical);

% filename of the multisine source used in the netlist
p.addParamValue('sourceFileName','MsSource',@ischar);

% name of the source component in the netlist
p.addParamValue('sourceName','MsSource_inst',@ischar);

% folder where the source is saved
p.addParamValue('sourceLocation',[],@ischar);

% number of allowed reruns for failed simulations
p.addParamValue('rerunCount', 10, @isnumeric)

p.StructExpand = true;
p.parse(NetlistFileName,MSdef,varargin{:});
args = p.ResultsMassaged();

% If the simulator is Harmonic Balance, check if args.HB_order and HB_freq have the same length
if(any(strcmpi(args.simulator,{'HB','ENV'})))
    if length(args.HB_freq)~= length(args.HB_freq)
        error('The vectors HB_freq and HB_order should have the same length')
    end
end

% Check the phase vector more thourougly
if ~isempty(args.phase)
    if ~iscell(args.phase)
        % also, overwrite the numberOfRealisations value based on the phase matrix
        [F,args.numberOfRealisations,S]=size(args.phase);
        if S<length(args.MSdef)
            error('phase should be a FxMxS matrix with F the amount of excited bins in the multisine.')
        end
        if F~=length(MSdef(1).grid)
            error('phase should be a FxMxS matrix with F the amount of excited bins in the multisine.')
        end
        clear F S
    else
        if length(args.phase)==length(args.MSdef)
            for ss=1:length(args.phase)
                [F,M] = size(args.phase{ss});
                % check whether the length of the grid corresponds to the length of the phase vector
                if F~=length(args.MSdef(ss).grid)
                    error('The phase cell array should contain FxM matrices where F is the amount of tones in the corresponding multisine');
                end
                if ss==1
                    % overwrite the numberOfRealisations
                    args.numberOfRealisations = M;
                else
                    if M~=args.numberOfRealisations
                        error('The phase cell array should contain FxM matrices where M is the same each time');
                    end
                end
            end
        else
            error('The length of the phase cell array should be the same as the MSdef array');
        end
    end
end

% check the amplitude vector thourougly
if ~isempty(args.amplitude)
    if length(args.amplitude)==length(args.MSdef)
        for ss=1:length(args.amplitude)
            [F,M] = size(args.amplitude{ss});
            % check whether the length of the grid corresponds to the length of the phase vector
            if F~=length(args.MSdef(ss).grid)
                error('The amplitude cell array should contain FxM matrices where F is the amount of tones in the corresponding multisine');
            end
            if ss==1
                % overwrite the numberOfRealisations
                args.numberOfRealisations = M;
            else
                if M~=args.numberOfRealisations
                    error('The amplitude cell array should contain FxM matrices where M is the same each time');
                end
            end
        end
    else
        error('The length of the amplitude cell array should be the same as the MSdef array');
    end
end

% check the DC vector thourougly
if ~isempty(args.DC)
    if length(args.DC)==length(args.MSdef)
        for ss=1:length(args.DC)
            [F,M] = size(args.DC{ss});
            % check whether the length of the grid corresponds to the length of the phase vector
            if F~=1
                error('The DC-offset can have only 1 dimension.');
            end
            if ss==1
                % overwrite the numberOfRealisations
                args.numberOfRealisations = M;
            else
                if M~=args.numberOfRealisations
                    error('The DC cell array should contain 1xM matrices where M is the same each time');
                end
            end
        end
    else
        error('The length of the DC cell array should be the same as the MSdef array');
    end
end

% if the OutFileName is present, make a cell array of outfilenames where a number is added to the end for each realisation
if ~isempty(args.OutFileName)
    outFileName = args.OutFileName;
    args.OutFileName = cell(args.numberOfRealisations,1);
    if args.numberOfRealisations>1
        for mm=1:args.numberOfRealisations
            args.OutFileName{mm} = [outFileName num2str(mm)];
        end
    else
        args.OutFileName{1} = outFileName;
    end
    clear outFileName
else
    args = rmfield(args,'OutFileName');
end

% if the InFileName is passed, make a cell array for each realisation as well
if ~isempty(args.InFileName)
    inFileName = args.InFileName;
    args.InFileName = cell(args.numberOfRealisations,1);
    if args.numberOfRealisations>1
        for mm=1:args.numberOfRealisations
            args.InFileName{mm} = [inFileName num2str(mm)];
        end
    else
        args.InFileName{1} = inFileName;
    end
    clear inFileName
else
    args = rmfield(args,'InFileName');
end

% if the sourceLocation is empty, use getpref to find the simulation folder
% and use that folder to store the excitation signals
if isempty(args.sourceLocation)
    args.sourceLocation=checkPrefs();
end

%% read the file into a cell array
% if it' a cell array already, just put it in data
data = args.NetlistFileName;


%% add the multisine source(s)
data{end+1}='';

% check whether the name field exists in the MSdef definition, if not, add the generic name to it.
if ~isfield(MSdef,'name')
    [MSdef.name] = deal('MultisineSource');
end

% add all the sources one by one and watch out not to have a collision between the different sources
for ii=1:length(args.MSdef)
    % the source should not be added to the netlist
    args.MSdef(ii).addSource = 0;
    % add a number to the netlistname to be able to separate the stuff
    args.MSdef(ii).name = [MSdef(ii).name '_' num2str(ii)];
    % the actual include will be added later
    
    % add the multisinesource itself
    if iscell(MSdef(ii).MSnode)
        if length(MSdef(ii).MSnode)==1
            % if the length is 1, connect the multisine between the node and ground
            data{end+1}=[MSdef(ii).name '_' num2str(ii) ':' args.sourceName num2str(ii) ' ' args.MSdef(ii).MSnode{1} ' 0'];%#ok
        elseif length(MSdef(ii).MSnode)==2
            % if 2 nodenames are provided, connect the source between the nodes
            data{end+1}=[MSdef(ii).name '_' num2str(ii) ':' args.sourceName num2str(ii) ' ' args.MSdef(ii).MSnode{1} ' ' args.MSdef(ii).MSnode{2}];%#ok
        else
            error('multisine sources can only contain 2 nodes')
        end
    else
        % if MSnode is a string, the source is connected between the node and ground
        data{end+1}=[MSdef(ii).name '_' num2str(ii) ':' args.sourceName num2str(ii) ' ' args.MSdef(ii).MSnode ' 0'];%#ok
    end
end
data{end+1}='';

%% add the simulation component
switch args.simulator
    case 'HB'
        if isempty(args.SimSettingsStruct)
            % if Freq and Order are provided, skip the calculation of the bins and use the user provided ones
            if(isempty(args.HB_freq))
                HBsettings = calculateHBSettings(args.MSdef,args.oversample);
            else
                HBsettings.Freq = args.HB_freq;
                HBsettings.Order = args.HB_order;
                HBsettings.MaxOrder = args.HB_order(1);
            end
            % add the OutputPlan to the settings
            HBsettings.OutputPlan = 'ADAMOUT';
            % The actual HB statement is added later, when the actual simulations are being performed
            % data{end+1} = ADSaddHB(-1,'HB1',HBsettings);
        else
            HBsettings = args.SimSettingsStruct;
        end
    case 'ENV'
        if isempty(args.SimSettingsStruct)
            if(isempty(args.HB_freq))
                % calculate the HBsettings like normal
                HBsettings = calculateHBSettings(args.MSdef,args.oversample);
                % but only keep the information about the carrier frequency
                HBsettings.Freq = HBsettings.Freq(2);
                HBsettings.Order = HBsettings.Order(2);
                HBsettings = rmfield(HBsettings,'MaxOrder');
            else
                HBsettings.Freq = args.HB_freq;
                HBsettings.Order = args.HB_order;
                %HBsettings.MaxOrder = args.HB_order(1);
                %Max mixing order should be at default value (4)
            end
            HBsettings.OutputPlan = 'ADAMOUT';
        else
            HBsettings = args.SimSettingsStruct;
        end
        HBsettings.SweepVar='time';
        HBsettings.SweepPlan='Env1_stim';
        HBsettings.EnvWarnPoorFit='yes';
        HBsettings.EnvUsePoorFit='yes';
        % TODO: this can probably be calculated automatically using the bandwidth of the multisine
        % Determines what fraction of the envelope bandwidth to use to
        % determine the fit. Only the frequency values that lie between
        % �0.5 x BandwidthFraction/Timestep around each carrier frequency
        % are used to determine the fit. 
        HBsettings.EnvBandwidth=args.EnvBandwidth;
        % if EnvIntegOrder is 2, trapezoidal integration is used
        % if EnvIntegOrder is 1, backwards euler is used
        % apparently, Piet had some trouble with trapezoidal integration
        HBsettings.EnvIntegOrder=args.EnvIntegOrder;
        % gear is bad! but use it anyway, because all the other integration
        % methods give weird results at high frequencies
        HBsettings.UseGear=args.EnvUseGear;
        % TODO: the SweepOffset parameter can be used to reduce the amount
        % of data sent to the mat file by not writing the output to file

        % The calculation of the time steps for the envelope simulation are
        % very similar to the one for a transient simulation
        binBW = (max(MSdef(1).grid)-min(MSdef(1).grid));
        if mod(binBW,2)==1;error('The amount of bins in the multisine should be odd');end;
        k = roundnicely(args.EnvOversample*binBW);
        fs = k*args.MSdef(1).f0;
        %keyboard
        EnvSweepSettings.Step = 1/fs;
        EnvSweepSettings.Start = 0;%args.periods/args.MSdef(1).f0;
        % I substract -1.5*Ts, because the envelope simulation has the
        % tendency to add one extra point
        EnvSweepSettings.Stop = args.periods/args.MSdef(1).f0-1.5/fs;
    case 'TRAN'
        if isempty(args.SimSettingsStruct)
            % the sample frequency fs is given by the maximum multisine frequency and the oversample rate
            % make sure the time step can be rounded nicely by rounding up the maximum frequency bin.
            % 93 bins has to become 100. We assume 1/f0 can be rounded nicely.
            k = roundnicely(2*args.oversample*max(args.MSdef(1).grid));
            fs = k*args.MSdef(1).f0;
            TRANsettings.MaxTimeStep = 1/fs;
            TRANsettings.MinTimeStep = 1/fs;
            TRANsettings.StartTime = 0;
            TRANsettings.StopTime = args.periods/args.MSdef(1).f0;
            TRANsettings.TimestepControl = 0; % TimestepControl = 0 gives a fixed time step
            TRANsettings.IntegMethod = 0; % IntegMethod=0 and Mu=0.5 gives trapezoidal integrations method
            TRANsettings.Mu = 0.5; %!!!!!!!!!!!!!!!!!!!!!!!! 0.5
            TRANsettings.OutputPlan = 'ADAMOUT';
            % The actual TRAN statement is added later, when the actual simulations are being performed
            % data{end+1} = ADSaddTran(-1,'Transient_simulation',TRANsettings);
        else
            TRANsettings = args.SimSettingsStruct;
            fs = 1/TRANsettings.MaxTimeStep;
        end
end

% if the OptionSettingsStruct is passed, use that one, otherwise, just add the topologycheck
if isempty(args.OptionSettingsStruct)
    % add the topology check to give some warnings is stuff is badly connected
    data{end+1} = ADSaddOptions(-1,'Options1','TopologyCheck','yes','TopologyCheckMessages','yes','GiveAllWarnings','yes');
else
    data{end+1} = ADSaddOptions(-1,'Options1',args.OptionSettingsStruct);
end

% add the output plan to make sure only the top level nodes are passed to the results
if isempty(args.SimSettingsStruct)
    %     data{end+1} = 'OutputPlan:ADAMOUT Type="Output" UseNodeNestLevel=yes NodeNestLevel=0 UseEquationNestLevel=yes EquationNestLevel=0 UseSavedEquationNestLevel=yes SavedEquationNestLevel=0 UseDeviceCurrentNestLevel=no DeviceCurrentNestLevel=0 DeviceCurrentDeviceType="All" DeviceCurrentSymSyntax=yes UseCurrentNestLevel=yes CurrentNestLevel=0 UseDeviceVoltageNestLevel=no DeviceVoltageNestLevel=0 DeviceVoltageDeviceType="All"';
    outputPlan.Type = 'Output';
    outputPlan.UseNodeNestLevel = 'yes';
    outputPlan.NodeNestLevel = args.outputNestLevel;
    outputPlan.UseEquationNestLevel = 'yes';
    outputPlan.EquationNestLevel = args.outputNestLevel;
    outputPlan.UseSavedEquationNestLevel='yes';
    outputPlan.SavedEquationNestLevel=args.outputNestLevel;
    outputPlan.UseDeviceCurrentNestLevel='no';
    outputPlan.DeviceCurrentNestLevel=args.outputNestLevel;
    outputPlan.DeviceCurrentDeviceType='All';
    outputPlan.DeviceCurrentSymSyntax='no';
    outputPlan.UseCurrentNestLevel='yes';
    outputPlan.CurrentNestLevel=args.outputNestLevel;
    outputPlan.UseDeviceVoltageNestLevel='no';
    outputPlan.DeviceVoltageNestLevel=args.outputNestLevel;
    outputPlan.DeviceVoltageDeviceType='All';
    data{end+1} = ADSaddOutputPlan(-1,'ADAMOUT',outputPlan);
else
    switch args.simulator
        case {'HB','ENV'}
            if ~isfield(HBsettings,'OutputPlan')
                HBsettings.OutputPlan = 'ADAMOUT';
                data{end+1} = ADSaddOutputPlan(-1,'ADAMOUT','Type','Output','DeviceCurrentSymSyntax','yes');
            else
                warning('You appear to be using an output plan.\r\n If you use custom filters for the MatlabOutput, make sure DeviceCurrentSymSyntax is set to yes.');%#ok
            end
        case 'TRAN'
            if ~isfield(TRANsettings,'OutputPlan')
                TRANsettings.OutputPlan = 'ADAMOUT';
                data{end+1} = ADSaddOutputPlan(-1,'ADAMOUT','Type','Output','DeviceCurrentSymSyntax','yes');
            else
                warning('You appear to be using an output plan.\r\n If you use custom filters for the MatlabOutput, make sure DeviceCurrentSymSyntax is set to yes.');%#ok
            end       
    end
end

%% simulate the different phase realisations

disp('Running the multisine simulations')
netlists = {};

% write the different multisines to their file
for mm=1:args.numberOfRealisations
    for jj=1:length(args.MSdef)
        % if the amplitude profile is weird, set the specific amplitude
        % profile for this multisine
        if ~isempty(args.amplitude)
            args.MSdef(jj).ampl=args.amplitude{jj}(:,mm);
        end
        % the phases have been specified in the phase matrix, use those,
        % otherwise, generate a random phase
        if ~isempty(args.phase)
            if ~iscell(args.phase)
                args.MSdef(jj).phase=args.phase(:,mm,jj);
            else
                args.MSdef(jj).phase=args.phase{jj}(:,mm);
            end
        else
            % generate a random phase, but make sure the crest factor is
            % below the specified CF
            attempt = 0;
            CF=Inf;
            while CF>=args.CF
                args.MSdef(jj).phase = 2*pi*rand(size(args.MSdef(jj).grid));
                period = MScalculatePeriod(args.MSdef(jj),16)-args.MSdef(jj).DC;
                CF = max(abs(period))/sqrt(mean(period.^2));
                attempt=attempt+1;
                if attempt>99
                    error('could not obtain the wanted crest factor with 100 random phase attempts, it is too strict')
                end
            end
        end
        % if needed, set the specific DC offset for the multisine
        if ~isempty(args.DC)
            args.MSdef(jj).DC=args.DC{jj}(1,mm);
        end
        % write the specific phase realisation to the multisine file
        MSwriteADS(args.MSdef(jj),fullfile(args.sourceLocation,[args.sourceFileName num2str(jj) '_' num2str(mm) '.net']));
    end
end

% Add the sweepplan if the ENVELOPE simulator was chosen
if strcmp(args.simulator,'ENV')
    %SweepSettings.Step=1/(40*length(args.MSdef(1).grid)*args.MSdef(1).f0);
    data{end+1}=ADSaddSweepPlan(-1,'Env1_stim',EnvSweepSettings);
end

for mm=1:args.numberOfRealisations
    % add the include for the multisine source(s)
    if mm==1
        includeLines = zeros(length(args.MSdef),1);
        % for the first realisation, no include has been added yet
        for jj=1:length(args.MSdef)
            data{end+1}=['#include "' fullfile(args.sourceLocation,[args.sourceFileName num2str(jj) '_' num2str(mm) '.net']) '"'];
            includeLines(jj)=length(data);
        end
    else
        % for the other realisations, overwrite the existing include
        for jj=1:length(args.MSdef)
            data{includeLines(jj)}=['#include "' fullfile(args.sourceLocation,[args.sourceFileName num2str(jj) '_' num2str(mm) '.net']) '"'];
        end
    end
    % add the simulation statement
    switch args.simulator
        case {'HB','ENV'}
            % if OutFileName exists, add the correct file to the HB statement
            if isfield(args,'OutFileName')
                HBsettings.OutFile = args.OutFileName{mm};
                HBsettings.UseOutFile = 'yes';
            end
            % if InFileName exists, add the correct file to the HB statement
            if isfield(args,'InFileName')
                HBsettings.InFile = args.InFileName{mm};
                HBsettings.UseInFile = 'yes';
            end
            if mm==1
                % for the first realisation, there's no sim statement yet
                data{end+1} = ADSaddHB(-1,'HARMONICBALANCE_sim',HBsettings);
            else
                % overwrite the sim statement in the other cases
                data{end} = ADSaddHB(-1,'HARMONICBALANCE_sim',HBsettings);
            end
        case 'TRAN'
            if mm==1
                % for the first realisation, there's no sim statement yet
                data{end+1} = ADSaddTran(-1,'TRANSIENT_sim',TRANsettings);
            else
                % overwrite the sim statement in the other cases
                data{end} = ADSaddTran(-1,'TRANSIENT_sim',TRANsettings);
            end
    end
    % write the netlists to the file system
    writeTextFile(data,['temp' num2str(mm) '.temp']);
    % and add the netlist to the list of netlists
    netlists{end+1} = ['temp' num2str(mm) '.temp'];
end

%try
    % run the simulations until all passed
    count = 0;
    allSims = (1:args.numberOfRealisations).';
    todoSims = allSims;
    SimRes_temp = cell(args.numberOfRealisations, 1);
    while (any(todoSims) && (count < args.rerunCount))
        
        % run the simulations
        switch nargout
            case {1,0}
                SimRes_sim = ADSrunSimulation(netlists(todoSims),args.customFilter,args.CleanupFiles,args.saveDatasets);
            case 2
                SimRes_sim = ADSrunSimulation(netlists(todoSims),args.customFilter,args.CleanupFiles,args.saveDatasets);
                rawSimData = SimRes_sim;
            case 3
                [SimRes_sim,superRawSimData] = ADSrunSimulation(netlists(todoSims),args.customFilter,args.CleanupFiles,args.saveDatasets);
                rawSimData = SimRes_sim;
        end
        
        % catch if todoSims only contains one element
        if isstruct(SimRes_sim)
            fields = fieldnames(SimRes_sim);
            if length(fields)>1
               SimRes_sim = rmfield(SimRes_sim,fields(2:end));
            end
            SimRes_temp(todoSims) = struct2cell(SimRes_sim);
        else
            for ii=1:length(SimRes_sim)
                fields = fieldnames(SimRes_sim{ii});
                if length(fields)>1
                    SimRes_sim{ii} = rmfield(SimRes_sim{ii},fields(2:end));
                end
            end
            SimRes_temp(todoSims) = SimRes_sim;
        end
        
        % get succeeded simulations
        failedSims = cellfun(@isempty, SimRes_temp);
        todoSims = allSims(failedSims);
        count = count + 1;
    end
    
    % change number of realisations if there are still failed simulations after
    % count overflow
    if any(todoSims)
       SimRes_temp(todoSims) = [];
       args.numberOfRealisations = args.numberOfRealisations - length(todoSims);
       warning(['Number of realisations changed to ', num2str(args.numberOfRealisations)])
    end
    
    % get the correct field out of the simulation result and save it in the SimRes matrix
    for ii=1:args.numberOfRealisations
        switch args.simulator
            case {'HB','ENV'}
                if isfield(SimRes_temp{ii}, 'sim1_HB')
                    SimRes(ii) = SimRes_temp{ii}.sim1_HB;
                else
                    SimRes(ii) = SimRes_temp{ii};
                end
            case 'TRAN'
                if isfield(SimRes_temp{ii}, 'sim1_TRAN')
                    SimRes(ii) = SimRes_temp{ii}.sim1_TRAN;
                else
                    SimRes(ii) = SimRes_temp{ii};
                end
        end
    end
    
% catch err
%     switch err.identifier
%         case 'ADS:simulate:simulationfailed'
%             error('SIMULATION FAILED, check the output of hpeesofsim.');
%         otherwise
%             error(['Error in encountered during simulation or loading: ' err.message])
%     end
% end
disp('Simulations done!')

% clean up the netlist
if args.CleanupFiles
    for mm=1:length(netlists)
        delete(netlists{mm});
    end
end

% if only the raw simulation data is needed, just break and give an empty spec struct back
if args.onlyRaw
    spec = [];
    return
end

%% create the spec structure, which contains M x N x F matrices for each node
fields = fieldnames(SimRes(1));
switch args.simulator
    case 'HB'
        % every signal is already in the frequency domain, this is easy
        freqset=0;
        for jj=1:length(fields)
            if ~strcmp(fields{jj},'freq')
                for mm=1:args.numberOfRealisations
                    % if a sweep is performed, only use the first of the results
                    if ~isvector(SimRes(mm).(fields{jj}))
                        spec.(fields{jj})(mm,:,:)=SimRes(mm).(fields{jj})(:,:);
                    else
                        spec.(fields{jj})(mm,:,:)=SimRes(mm).(fields{jj});
                    end
                end
            else
                if strcmp(fields{jj},'freq') && ~freqset
                    spec.freq=SimRes(1).freq;
                    freqset=1;
                end
            end
        end
    case 'ENV'
        % every signal is a NxF matrix. N is the amount of time-domain
        % samples, while F is the amount of harmonics in the harmonic
        % balance simulation. These matrices should be reshaped into 
        % MxPxNxF matrices and then converted into the frequency domain if
        % needed
        freqset=0;
        N = fs/args.MSdef(1).f0; % N is the amount of samples per period
        for jj=1:length(fields)
            if ~strcmp(fields{jj},{'freq','sweepvar_time'})
                % reshape the results into an MxPxTxF struct
                for mm=1:args.numberOfRealisations
                    for pp=floor(args.periods):-1:1
                        spec.(fields{jj})(mm,floor(args.periods)-(pp-1),:,:)=SimRes(mm).(fields{jj})(end-(pp)*N+1:end-(pp-1)*N,:);
                    end
                end
                
                % if needed, transform the results to the frequency domain
                if strcmp(args.EnvDomain,'freq')
                    % take the fft of each of the complex time domain signals
                    temp = fftshift(fft(spec.(fields{jj}),[],3)/size(spec.(fields{jj}),3),3);
                    [M,P,N,F] = size(temp);
                    % the final spectrum has N bins around each harmonic
                    % and a half spectrum around DC
                    spec.(fields{jj}) = zeros(M,P,(F-1)*N+ceil(N/2));
                    % Put DC first. There the spectrum is not complex, so we
                    % only need half of the spectrum. 
                    spec.(fields{jj})(:,:,1:(ceil(N/2))) = temp(:,:,floor(N/2)+1:end,1);
                    % Correct also for the fact that HB returns a
                    % single-sided spectrum but DC should not get a factor 2 
                    spec.(fields{jj})(:,:,2:(ceil(N/2))) = 2*spec.(fields{jj})(:,:,2:(ceil(N/2)));
                    % now do the other frequencies
                    spec.(fields{jj})(:,:,ceil(N/2)+1:end) = reshape(temp(:,:,:,2:end),[M P (F-1)*N]);
                end
            else
                if strcmp(fields{jj},'freq') && ~freqset
                    if strcmp(args.EnvDomain,'freq')
                        % build the frequency axis of the reformed spectrum
                        F = length(SimRes(1).freq);
                        spec.freq = zeros(1,(F-1)*N+ceil(N/2));
                        spec.freq(1:ceil(N/2)) = args.MSdef(1).f0*(0:ceil(N/2)-1);
                        
                        dummy=(fs/N)*(-ceil((N-1)/2):floor((N-1)/2));
                        % if needed, compensate for the frequency warping in the transient simulation
                        if args.freqWarping && args.EnvIntegOrder==2 && args.EnvUseGear==0
                                dummy=(fs/pi)*tan(pi*dummy/fs);
                        end
                        
                        for ff=2:F
                            rng = ceil(N/2)+((N*(ff-2)+1):(N*(ff-1)));
                            spec.freq(rng) = SimRes(1).freq(ff) + dummy;
                        end
                        freqset=1;
                    else
                        % just pass on the time domain signals and the frequencies of the fundamentals
                        spec.time=SimRes(1).sweepvar_time;
                        spec.freq=SimRes(1).freq;
                        freqset=1;
                    end
                end
            end
        end
    case 'TRAN'
        % every signal is in the time domain, we have to throw away the transients,
        % cut it into pieces and transform the signals to the frequency domain.
        freqset=0;
        periodsamples = fs/args.MSdef(1).f0;
        for ii=1:length(fields)
            if ~strcmp(fields{ii},'time')
                %                 if ~args.useBatch
                for mm=1:args.numberOfRealisations
                    for pp=floor(args.periods):-1:1
                        spec.(fields{ii})(mm,floor(args.periods)-(pp-1),:)=SimRes(mm).(fields{ii})(end-(pp)*periodsamples+1:end-(pp-1)*periodsamples);
                    end
                end
                %                 else
                %                     for mm=1:args.numberOfRealisations
                %                         for pp=1:floor(args.periods)
                %                             spec.(fields{ii})(mm,floor(args.periods)-(pp-1),:)=SimRes.(fields{ii})(mm,end-(pp)*periodsamples+1:end-(pp-1)*periodsamples);
                %                         end
                %                     end
                %                 end
                spec.(fields{ii}) = fft(spec.(fields{ii}),[],3)/size(spec.(fields{ii}),3);
                % remove the top side of the spectrum, this function returns a single-sided spectrum only
                spec.(fields{ii}) = 2*spec.(fields{ii})(:,:,1:ceil(end/2));
                spec.(fields{ii})(:,:,1) = spec.(fields{ii})(:,:,1)/2;
            else
                if ~freqset
                    spec.freq=(0:periodsamples-1)*args.MSdef(1).f0;
                    % if needed, compensate for the frequency warping in the transient simulation
                    if args.freqWarping
                        spec.freq = (fs/pi)*tan(pi*spec.freq/fs);
                    end
                    spec.freq = spec.freq(1:ceil(end/2));
                    freqset=1;
                end
            end
        end
end

end

%% function to check the MSdef struct passed to the function
function res = checkMSdef(s)
check={'f0' @isscalar;...
    'grid' @isvector;...
    'ampl' @isvector;...
    'Rout' (@(x) isscalar(x) && x>=0);...
    'MSnode' (@(x) ischar(x) || iscell(x))};

for jj=1:length(s)
    if isstruct(s(jj))
        for ii=1:length(check)
            if isfield(s(jj),check{ii,1});
                if ~check{ii,2}(s(jj).(check{ii,1}))
                    error(['MSdef.' check{ii,1} ' is not right']);
                end
            else
                error(['MSdef needs the field: ' check{ii,1}])
            end
        end
        res = 1;
    else
        error('MSdef should be a struct');
    end
end
end

% @generateFunctionHelp

% @Tagline calculates the response of the system to the multisine. Voltages and currents at the stage interfaces are saved.

% @Description In the following, we use F to indicate the amount of tones
% @Description in the multisine, M for the amount of realisations to be simulated and S
% @Description for the amount of multisine sources in each realisation.
% @Description Note: only the first multisine is used to calculate the simulation parameters for now. 
% @Description The function works best when the frequency grids of the multisines are the same.

% @Outputs{1}.Description{1} Contains the response of the circuit to the multisine. The
% @Outputs{1}.Description{2} struct contains several MxPxF matrices with the calculated spectra of
% @Outputs{1}.Description{3} each named node or current in the circuit
% @Outputs{1}.Type struct

% @Outputs{2}.Description{1} Contains the raw simulation data as loaded by the
% @Outputs{2}.Description{2} ADSimportSimData function. It is a vector of length M
% @Outputs{2}.Type cell array of structs

% @Outputs{3}.Description{1} Contains the raw simulation data as it is put in the .mat file
% @Outputs{3}.Description{2} by the matlaboutput function of ADS
% @Outputs{3}.Type cell array of structs

% @SeeAlso ADSrunSimulation
% @SeeAlso ADSimportSimData
% @SeeAlso ADSsimulateMSwaves




