function [W,DCOP] = ADSsizeTransistor(BasicNetlist,varargin)
% this function calculates the width of a transistor to obtain the wanted Id or Gm
%
%   [W,DCOP] = ADSsizeTransistor(BasicNetlist,'ParamName',ParamValue)
%
% BasicNetlist is a netlist that contains one transistor and the reference
% to the correct netlist. The transistor has a witdh W and a length L (the 
% function will add the variable statements itself). The transistor is 
% connected as follows:
%
%   NMOS       o drain    |   PMOS       o source  
%              |          |              |          
%           ||--          |           ||<-          
%   gate o--||-----o bulk |   gate o--||-----o bulk 
%           ||->          |           ||--          
%              |          |              |          
%              o source   |              o drain   
% 
% Parameter/Value pairs
%  Gm         []    Wanted transconductance Gm of the transistor.
%  Id         []    Wanted drain current of the transistor. You can either
%                   Gm or Id, if you set both, the transistor is sized for 
%                   the provided Gm.
%  Vov        0.2   Overdrive voltage of the transistor. If you set Vg, the
%                   overdrive is ignored.
%  L          1e-6  gate length of the transistor
%  Vs         0     source voltage of the transistor
%  Vd         1     drain voltage of the transistor
%  Vb         0     bulk voltage of the transistor
%  Vg         []    gate voltage of the transistor. If this is unset, the
%                   function calculated the Vt and applies Vt+Vov to the gate
%  Wref       5e-6  Width of the reference transistor used to calculate the
%                   final width
%  Error      0.1   Maximum relative error allowed on the obtained Gm or Id
% 
% Adam Cooman, ELEC VUB
% 06/08/2014    Version 1

p = inputParser();
p.addRequired('BasicNetlist',@iscell)
% wanted values
p.addParamValue('Gm',[],@isscalar);
p.addParamValue('Id',[],@isscalar);
% design parameters
p.addParamValue('Vov',0.2,@isscalar);
p.addParamValue('L',1e-6,@isscalar);
% voltages around the transistor
p.addParamValue('Vs',0,@(x) isscalar(x)|isempty(x) );
p.addParamValue('Vb',0,@(x) isscalar(x)|isempty(x) );
p.addParamValue('Vd',1,@isscalar);
p.addParamValue('Vg',[],@isscalar);
% transistor type
p.addParamValue('Type','NMOS',@ischar);
% reference width
p.addParamValue('Wref',5e-6,@isscalar);
% maximally tolerated relative error on the result
p.addParamValue('Error',0.005,@isscalar);
% Vgs voltage applied when determining the Vt
p.addParamValue('VgsTest',1,@isscalar)
p.parse(BasicNetlist,varargin{:});
args = p.Results();
clear p BasicNetlist varargin

% find out whether Gm is wanted or Id is wanted
if ~isempty(args.Gm)
    mode = 'Gm';
elseif ~isempty(args.Id)
    mode = 'Id';
else
    error('You should either provide a wanted Gm or Id');
end

% add the W and L variables
args.BasicNetlist{end+1} = ['L=' eng(args.L)];
args.BasicNetlist{end+1} = ['W=' eng(args.Wref)];
WidthLine = length(args.BasicNetlist);

% add the Dc simulation and make sure the device operating point is returned
args.BasicNetlist{end+1} = ADSaddDC(-1,'DC_sim','DevOpPtLevel',4);

% add the different voltage sources to bias the transistor correctly
if isempty(args.Vg)
    % If the Vg is not specified, we have to calculate the threshold voltage first
    % we do a first simulation with a random Vgs of 1 Volt, determine the Vt
    % and them apply the correct gate voltage later on
    args.BasicNetlist{end+1} = ADSaddV_Source(-1,'V_bulk'  ,'p','bulk'  ,'n','0','Type','V_DC','Vdc',args.Vb);
    args.BasicNetlist{end+1} = ADSaddV_Source(-1,'V_drain' ,'p','drain' ,'n','0','Type','V_DC','Vdc',args.Vd);
    args.BasicNetlist{end+1} = ADSaddV_Source(-1,'V_source','p','source','n','0','Type','V_DC','Vdc',args.Vs);
    switch args.Type
        case 'NMOS'
            args.BasicNetlist{end+1} = ADSaddV_Source(-1,'V_gateSource','p','gate','n','source','Type','V_DC','Vdc',args.VgsTest);
        case 'PMOS'
            args.BasicNetlist{end+1} = ADSaddV_Source(-1,'V_gateSource','p','source','n','gate','Type','V_DC','Vdc',args.VgsTest);
    end
    gateVoltageLine = length(args.BasicNetlist);
    
    % simulate this for a first time
    writeTextFile(args.BasicNetlist,'SimTemp.net');
    simres = ADSrunSimulation('SimTemp.net');
    % get the threshold voltage of the transistor and set the correct overdrive
    Vt = simres.DCOP.MOSFET_1.Vth;
    % put the Vg to Vt+Vov
    switch args.Type
        case 'NMOS'
            args.BasicNetlist{gateVoltageLine} = ADSaddV_Source(-1,'V_gateSource','p','gate','n','source','Type','V_DC','Vdc',abs(Vt) + args.Vov);
        case 'PMOS'
            args.BasicNetlist{gateVoltageLine} = ADSaddV_Source(-1,'V_gateSource','p','source','n','gate','Type','V_DC','Vdc',abs(Vt) + args.Vov);
    end
elseif isempty(args.Vs)
    disp('hello')
    % if the source voltage is not specified, we calculate the threshold
    % voltage and make sure Vgs is correct for the specified overdrive
    args.BasicNetlist{end+1} = ADSaddV_Source(-1,'V_drain' ,'p','drain' ,'n','0','Type','V_DC','Vdc',args.Vd);
    args.BasicNetlist{end+1} = ADSaddV_Source(-1,'V_gate'  ,'p','gate'  ,'n','0','Type','V_DC','Vdc',args.Vg);
    if ~isempty(args.Vb)
        args.BasicNetlist{end+1} = ADSaddV_Source(-1,'V_bulk','p','bulk'  ,'n','0','Type','V_DC','Vdc',args.Vb);
    else
        switch args.Type
            case 'NMOS'
                args.BasicNetlist{end+1} = ADSaddV_Source(-1,'V_gateBulk','p','gate','n','bulk','Type','V_DC','Vdc',args.VgsTest);
            case 'PMOS'
                args.BasicNetlist{end+1} = ADSaddV_Source(-1,'V_gateBulk','p','bulk','n','gate','Type','V_DC','Vdc',args.VgsTest);
        end
        gatebulkVoltageLine = length(args.BasicNetlist);
    end
    % add the temporary Gate-source voltage source
    switch args.Type
        case 'NMOS'
            args.BasicNetlist{end+1} = ADSaddV_Source(-1,'V_gateSource','p','gate'  ,'n','source','Type','V_DC','Vdc',args.VgsTest);
        case 'PMOS'
            args.BasicNetlist{end+1} = ADSaddV_Source(-1,'V_gateSource','p','source'  ,'n','gate','Type','V_DC','Vdc',args.VgsTest);
    end
    gatesourceVoltageLine = length(args.BasicNetlist);
    
    % run a first simulation to calculate Vt
    writeTextFile(args.BasicNetlist,'SimTemp.net');
    simres = ADSrunSimulation('SimTemp.net');
    % get the threshold voltage of the transistor and set the correct overdrive
    Vt = abs(simres.DCOP.MOSFET_1.Vth);
    
    % now add the correct Vgs
    switch args.Type
        case 'NMOS'
            args.BasicNetlist{gatesourceVoltageLine} = ADSaddV_Source(-1,'V_gateSource','p','gate'  ,'n','source','Type','V_DC','Vdc',Vt + args.Vov);
        case 'PMOS'
            args.BasicNetlist{gatesourceVoltageLine} = ADSaddV_Source(-1,'V_gateSource','p','source'  ,'n','gate','Type','V_DC','Vdc',Vt + args.Vov);
    end
    
    % and, if needed, add the Vgb source
    if isempty(args.Vb)
        switch args.Type
            case 'NMOS'
                args.BasicNetlist{gatebulkVoltageLine} = ADSaddV_Source(-1,'V_gateBulk','p','gate','n','bulk','Type','V_DC','Vdc',Vt + args.Vov);
            case 'PMOS'
                args.BasicNetlist{gatebulkVoltageLine} = ADSaddV_Source(-1,'V_gateBulk','p','bulk','n','gate','Type','V_DC','Vdc',Vt + args.Vov);
        end
    end
    
else
    % all voltages around the transistor are specified, just put the sources in the correct location, simulation follows later
    args.BasicNetlist{end+1} = ADSaddV_Source(-1,'V_bulk'  ,'p','bulk'  ,'n','0','Type','V_DC','Vdc',args.Vb);
    args.BasicNetlist{end+1} = ADSaddV_Source(-1,'V_drain' ,'p','drain' ,'n','0','Type','V_DC','Vdc',args.Vd);
    args.BasicNetlist{end+1} = ADSaddV_Source(-1,'V_source','p','source','n','0','Type','V_DC','Vdc',args.Vs);
    args.BasicNetlist{end+1} = ADSaddV_Source(-1,'V_gate'  ,'p','gate'  ,'n','0','Type','V_DC','Vdc',args.Vg);
end

ERROR = Inf;
W = args.Wref;
while ERROR > args.Error
    % put the new length in the netlist
    args.BasicNetlist{WidthLine} = ['W=' eng(W)];
    
    % run the simulation with the correct gate voltage, L and Wref
    writeTextFile(args.BasicNetlist,'SimTemp.net');
    simres = ADSrunSimulation('SimTemp.net');
    DCOP = simres.DCOP.MOSFET_1;
    
    switch mode
        case 'Gm'
            Gm = DCOP.Gm;
%             disp(['found a Gm of: ' num2str(Gm)]);
%             disp(['wanted Gm is: ' num2str(args.Gm)]);
            ERROR = abs(args.Gm - Gm)/abs(args.Gm);
%             disp(['The error is: ' num2str(ERROR)]);
            scale = args.Gm/Gm;
%             disp(['correcting with a scale factor of: ' num2str(scale)]);
        case 'Id'
            Id = abs(DCOP.Id);
%             disp(['found a Id of: ' num2str(Id)]);
%             disp(['wanted Id is: ' num2str(args.Id)]);
            ERROR = abs(args.Id - Id)/abs(args.Id);
%             disp(['The error is: ' num2str(ERROR)]);
            scale = args.Id/Id;
%             disp(['correcting with a scale factor of: ' num2str(scale)]);
    end
    
    % scale the transistor such that the gm will be correct
    W = W * scale;
    
%     disp(['trying with a W of: ' num2str(W)]);
    
end

% clean up the rubbish you made
delete('SimTemp.net')


end