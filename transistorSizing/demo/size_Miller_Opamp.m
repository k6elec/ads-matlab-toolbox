function [Netlist,DCOP] = size_Miller_Opamp(varargin)


% TODO: find an elegant way to change the outputtransistors to another technology
% TODO: make the current mirror and the  transistor to be different in the amount of fingers only
% TODO: allow an NMOS input pair

p=inputParser();
% load capacitor
p.addParamValue('Cload',2e-12,@isscalar);
% gain-bandwidth product
p.addParamValue('GBW',10e6,@isscalar);
% ratio between the GBW and the non-dominant pole
p.addParamValue('gamma',2,@isscalar);
% ratio between the compensation capacitor and the load capacitor
p.addParamValue('alpha',3,@isscalar);
% guess for the ratio between the gate-source capacitance of the output transistor and
% the compensation capacitor
p.addParamValue('beta',3,@isscalar);
% length of the input transistor
p.addParamValue('Lin',2e-6,@isscalar);
% gatelength of the nmos output transistor. The mirror transistors will
% have the same gatelength.
p.addParamValue('Lout_N',1e-6,@isscalar);
% gatelength of the pmos output transistor
p.addParamValue('Lout_P',[],@isscalar);
% gatelength of the top transistor
p.addParamValue('Ltop',1e-6,@isscalar);
% overdrive voltage of the NMOS output transistor. Due to the architecture
% of the Miller op-amp, the mirror transistors will have the same overdrive
% voltage.
p.addParamValue('Vovout_N',0.2,@isscalar);
% overdrive voltage of the load transistor
p.addParamValue('Vovout_P',[],@isscalar);
% overdrive voltage of the top transistor
p.addParamValue('Vovtop',[],@isscalar);
% overdrive of the input transistors
p.addParamValue('Vovin',0.15,@isscalar);
% Supply voltage of the op-amp
p.addParamValue('Vsupply',3.3,@isscalar);
% small-signal ground voltage. If this one is left empty, the acgnd voltage
% is half of the supply voltage.
p.addParamValue('acgnd',[],@isscalar);
% if this is set to true, the op-amp is placed in a subcircuit. If the
% op-amp is a sub-circuit, you need to add the include statement externally
p.addParamValue('subcircuit',true,@islogical);
% basic netlist of the NMOS transistors used in the miller op-amp
p.addParamValue('NMOS',[],@iscellstr)
% basic netlist of the PMOS transistor used in the miller op-amp
p.addParamValue('PMOS',[],@iscellstr)
% if this is set, current sources are added to the netlist to allow
% estimation of the BLA of each sub-circuit. and to allow a DCA. The op-amp
% is split into a bias network, an input pair combined with a current
% mirror (positive input is assumed to be connected to acgnd) and an output transistor
p.addParamValue('addCurrentSources',false,@islogical);
% node name of the output node
p.addParamValue('outNodeName','out',@ischar);
% node name of the negative input
p.addParamValue('inpNodeName','inp',@ischar);
% node name of the positive intput
p.addParamValue('inmNodeName','inm',@ischar);
p.parse(varargin{:});
args = p.Results;
clear p varargin

% the default netlists that contain the correct transistors
if ~isempty(args.NMOS)
    NMOS = args.NMOS;
else
    error('you should pass a test netlist with a NMOS to the function')
end
if ~isempty(args.PMOS)
    PMOS = args.PMOS;
else
    error('you should pass a test netlist with a PMOS to the function')
end

% do some more default values on the transistor parameters
% default length of the pmos load of the output transistor is the same as the NMOS output
if isempty(args.Lout_P)
    args.Lout_P = args.Lout_N;
end
% default length and overdrive of the PMOS transistors is equal
if isempty(args.Ltop)
    args.Ltop = args.Lout_P;
end
if isempty(args.Vovout_P)
    args.Vovout_P = args.Vovout_N;
end

% Apply the design plan of Sansen
% non-dominant pole
args.fnd = args.gamma*args.GBW;
% approximate the location of the non-dominant pole to get an estimate of gm2
args.gm2 = args.fnd * 2 * pi * args.Cload;
% compensation capacitor
args.Cc = args.Cload/args.alpha;
% transconductance of the input transistor
args.gm1 = args.GBW*2*pi*args.Cc;
if isempty(args.acgnd)
    args.acgnd = args.Vsupply/2;
end

Netlist{1}='';
% the include statement for the models
if args.subcircuit
    Netlist{end+1} = sprintf('define MillerOpamp( %s %s %s ) ',args.inpNodeName,args.inmNodeName,args.outNodeName);
end

% supply voltage
Netlist{end+1} = ADSaddV_Source(-1,'Vsupply','p','vdd','n','0','Type','V_DC','Vdc',args.Vsupply);
% load capacitor
% Netlist{end+1} = ADSaddC(-1,'Cl','p','out','n','0','C',args.Cl);
% size the output transistor
[W_out,DCOP.out] = ADSsizeTransistor(NMOS,'Type','NMOS','Gm',args.gm2,'L',args.Lout_N,'Vov',args.Vovout_N,'Vd',args.acgnd,'Vs',0,'Vb',0);
% size the load transistor
[W_load,DCOP.load] = ADSsizeTransistor(PMOS,'Type','PMOS','Id',abs(DCOP.out.Id),'L',args.Lout_P,'Vov',args.Vovout_P,'Vd',args.acgnd,'Vs',args.Vsupply,'Vb',args.Vsupply);
if args.addCurrentSources
    Netlist{end+1} = ADSaddShort(-1,'I_OUT_p2','sink',args.outNodeName,'source','V_OUT_p2','savecurrent','yes','Mode',0);
    Netlist{end+1} = ADSaddShort(-1,'I_OUT_p1','sink','int','source','V_OUT_p1','savecurrent','yes','Mode',0);
    Netlist{end+1} = sprintf('"n_33_mm":Mout V_OUT_p2 V_OUT_p1 0 0 Length=%s Width=%s',eng(args.Lout_N),eng(W_out));
    Netlist{end+1} = sprintf('"p_33_mm":Mload V_OUT_p2 biasload vdd vdd Length=%s Width=%s ',eng(args.Lout_P),eng(W_load));
else
    Netlist{end+1} = sprintf('"p_33_mm":Mload %s biasload vdd vdd Length=%s Width=%s ',args.outNodeName,eng(args.Lout_P),eng(W_load));
    Netlist{end+1} = sprintf('"n_33_mm":Mout %s int 0 0 Length=%s Width=%s',args.outNodeName,eng(args.Lout_N),eng(W_out));
end
% and add the bias network for the load transistor
Netlist{end+1} = sprintf('"p_33_mm":Mloadbias biasload biasload vdd vdd Length=%s Width=%s ',eng(args.Lout_P),eng(W_load));
Netlist{end+1} = ADSaddI_Source(-1,'Ibiasload','source','0','sink','biasload','Idc',abs(DCOP.load.Id));

% calculate the gate source capacitance by summing the different Dqg parameters from the transcapacitance matrix
Cint = DCOP.out.DqgDvgb;
if Cint>args.Cc
    warning('gate-source capacitor of the output transistor is bigger than the compensation capacitor')
end
% compensation capacitor
Netlist{end+1} = ADSaddC(-1,'Cc','p','out','n','int','C',args.Cc);

% input transistor
[W_in,DCOP.in] = ADSsizeTransistor(PMOS,'Type','PMOS','Gm',args.gm1,'L',args.Lin,'Vd',DCOP.out.Vgs,'Vs',[],'Vb',[],'Vg',args.acgnd,'Vov',args.Vovin);
% size the top transistor
[W_top,DCOP.top] = ADSsizeTransistor(PMOS,'Type','PMOS','Id',2*abs(DCOP.in.Id),'L',args.Ltop,'Vov',args.Vovout_N,'Vd',args.acgnd+abs(DCOP.in.Vgs),'Vs',args.Vsupply,'Vb',args.Vsupply);
% size the mirror transistor. It has the same overdrive of the output
% transistor and the same current as the input transistor
[W_mirr,DCOP.mirr] = ADSsizeTransistor(NMOS,'Type','NMOS','Id',abs(DCOP.in.Id),'L',args.Lout_N,'Vov',args.Vovout_N,'Vd',DCOP.out.Vgs,'Vs',0,'Vb',0);


if args.addCurrentSources
    Netlist{end+1} = ADSaddShort(-1,'I_IN_p1','sink',args.inmNodeName,'source','V_IN_p1','savecurrent','yes','Mode',0);
    Netlist{end+1} = ADSaddShort(-1,'I_IN_p2','sink','int','source','V_IN_p2','savecurrent','yes','Mode',0);
    Netlist{end+1} = sprintf('"p_33_mm":Minp V_IN_p2  %s top top Length=%s Width=%s ',args.inpNodeName,eng(args.Lin),eng(W_in));
    Netlist{end+1} = sprintf('"p_33_mm":Minm mirr V_IN_p1 top top Length=%s Width=%s ',eng(args.Lin),eng(W_in));
    Netlist{end+1} = sprintf('"n_33_mm":Mmirrp V_IN_p2  mirr 0 0 Length=%s Width=%s',eng(args.Lout_N),eng(W_mirr)); 
else
    Netlist{end+1} = sprintf('"p_33_mm":Minp int  %s top top Length=%s Width=%s ',args.inpNodeName,eng(args.Lin),eng(W_in));
    Netlist{end+1} = sprintf('"p_33_mm":Minm mirr %s top top Length=%s Width=%s ',args.inmNodeName,eng(args.Lin),eng(W_in));
    Netlist{end+1} = sprintf('"n_33_mm":Mmirrp int  mirr 0 0 Length=%s Width=%s',eng(args.Lout_N),eng(W_mirr));
end
% add the top transistor
Netlist{end+1} = sprintf('"p_33_mm":Mtop top biastop vdd vdd Length=%s Width=%s ',eng(args.Ltop),eng(W_top));
% just add the top transistor again as a bias transistor and a current source
Netlist{end+1} = sprintf('"p_33_mm":Mtopbias biastop biastop vdd vdd Length=%s Width=%s ',eng(args.Ltop),eng(W_top));
Netlist{end+1} = ADSaddI_Source(-1,'Ibiastop','source','0','sink','biastop','Idc',abs(DCOP.top.Id));
% add the diode connected transistor in the current mirror
Netlist{end+1} = sprintf('"n_33_mm":Mmirrm mirr mirr 0 0 Length=%s Width=%s',eng(args.Lout_N),eng(W_mirr));

if args.subcircuit
    Netlist{end+1} = 'end MillerOpamp';
end

end

% @generateFunctionHelp

% @tagline Sizes a miller op-amp in ADS

% @output1 ADS netlist that contains the two-stage miller op-amp.
% @outputType1 cell array of strings