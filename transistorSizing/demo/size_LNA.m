% this script sizes a shunt feedback LNA
clc
close all
clear all


NMOS = readTextFile('NMOS_1.8V_basicNetlist.net');
PMOS = readTextFile('PMOS_1.8V_basicNetlist.net');

Netlist{1} = '';
Netlist{end+1}='#define TT TT';
Netlist{end+1}='#ifndef inc_Z__UMC_180_models_umc18config_net';
Netlist{end+1}='#define inc_Z__UMC_180_models_umc18config_net inc_Z__UMC_180_models_umc18config_net';
Netlist{end+1}='#include "Z:\UMC_180\models\umc18config.net"';
Netlist{end+1}='#endif';
Netlist{end+1}='#undef TT';
Netlist{end+1}='';

% "p_18_mm":Mload  drain gate source bulk Length=%s Width=%s Ad=2*hdif*%s As=2*hdif*%s Pd=4*hdif+2*%s Ps=4*hdif+2*%s
% "n_18_mm":Mtest  drain gate source bulk Length=%s Width=%s Ad=2*hdif*%s As=2*hdif*%s Pd=4*hdif+2*%s Ps=4*hdif+2*%s

Netlist{end+1} = ADSaddV_Source(-1,'Vsupply','p','vdd','n','0','Type','V_DC','Vdc',3.3);

Lmin = 0.18e-6; % mimimum gate length of the technology
hdif = 0.26e-6; % minimum spacing at the drain connection
Netlist{end+1} = sprintf('hdif=%s',eng(hdif));
Z0 = 50; % input impedance
Rl = 300; % load resistor at the top

Rf = 300; % feedback resistor
Netlist{end+1} = ADSaddR(-1,'Rfb','p','buff','n','V_TOTAL_p1','R',Rf);
Cl = 0.75e-12; % load capacitor
Netlist{end+1} = ADSaddC(-1,'Cload','p','out','n','0','C',Cl);

d=0.2; % voltage headroom at the cascode node

Vout = 1.2; % DC voltage level of the output node
Vdd = 1.8; % supply voltage
Ires = (Vdd-Vout)/Rl; % current through the load resistor

Netlist{end+1} = ADSaddShort(-1,'I_TOTAL_p1','sink','in','source','V_TOTAL_p1','Mode',0,'SaveCurrent','yes');
Netlist{end+1} = ADSaddShort(-1,'I_TOTAL_p2','sink','out','source','V_TOTAL_p2','Mode',0,'SaveCurrent','yes');

% first size the input transistor
gm1 = 0.03;
Vovin = 0.5;
[W_in,DCOP_in] = ADSsizeTransistor(NMOS,'Type','NMOS','L',Lmin,'Vov',Vovin,'Gm',gm1,'Vs',0,'Vb',0,'Vd',Vovin+d);
% Netlist{end+1} = sprintf('"n_18_mm":Min casc in 0 0 Length=%s Width=%s Ad=2*hdif*%s As=2*hdif*%s Pd=4*hdif+2*%s Ps=4*hdif+2*%s',eng(Lmin),eng(W_in),eng(W_in),eng(W_in),eng(W_in),eng(W_in));
Netlist{end+1} = ADSaddMOSFET(-1,'"n_18_mm"','Min','source','0','drain','V_IN_p2','gate','V_IN_p1','bulk','0',...
    'Length',Lmin,'Width',W_in,'As',2*hdif*W_in,'Ad',2*hdif*W_in,'Ps',2*hdif+2*W_in,'Pd',2*hdif+2*W_in);
Netlist{end+1} = ADSaddShort(-1,'I_IN_p1','sink','V_TOTAL_p1','source','V_IN_p1','Mode',0,'SaveCurrent','yes');
Netlist{end+1} = ADSaddShort(-1,'I_IN_p2','sink','casc','source','V_IN_p2','Mode',0,'SaveCurrent','yes');


% now the cascode transistor
[W_casc,DCOP_casc] = ADSsizeTransistor(NMOS,'Type','NMOS','L',Lmin,'Id',abs(DCOP_in.Id),'Vs',Vovin+d,'Vb',0,'Vd',Vout,'Vg',1.8);
% Netlist{end+1} = sprintf('"n_18_mm":Mcasc out vdd casc 0 Length=%s Width=%s Ad=2*hdif*%s As=2*hdif*%s Pd=4*hdif+2*%s Ps=4*hdif+2*%s',eng(Lmin),eng(W_casc),eng(W_casc),eng(W_casc),eng(W_casc),eng(W_casc));
Netlist{end+1} = ADSaddMOSFET(-1,'"n_18_mm"','Mcasc','source','V_CASC_p1','drain','V_CASC_p2','gate','vdd','bulk','0',...
    'Length',Lmin,'Width',W_casc,'As',2*hdif*W_casc,'Ad',2*hdif*W_casc,'Ps',2*hdif+2*W_casc,'Pd',2*hdif+2*W_casc);
Netlist{end+1} = ADSaddShort(-1,'I_CASC_p1','sink','casc','source','V_CASC_p1','Mode',0,'SaveCurrent','yes');
Netlist{end+1} = ADSaddShort(-1,'I_CASC_p2','sink','V_TOTAL_p2' ,'source','V_CASC_p2','Mode',0,'SaveCurrent','yes');

Iload = abs(DCOP_in.Id)-Ires; % current through the top PMOS transistor

% size the top PMOS
Lpmos = 0.5e-6;
[W_pmos,DCOP_pmos] = ADSsizeTransistor(PMOS,'Type','PMOS','L',Lpmos,'Vov',0.5,'Id',Iload,'Vs',1.8,'Vb',1.8,'Vd',Vout);
Netlist{end+1} = ADSaddMOSFET(-1,'"p_18_mm"','Mpmos','source','vdd','drain','V_PMOS_p1','gate','pmosbias','bulk','vdd',...
    'Length',Lpmos,'Width',W_pmos,'As',2*hdif*W_pmos,'Ad',2*hdif*W_pmos,'Ps',2*hdif+2*W_pmos,'Pd',2*hdif+2*W_pmos);
Netlist{end+1} = ADSaddMOSFET(-1,'"p_18_mm"','Mpmosbias','source','vdd','drain','pmosbias','gate','pmosbias','bulk','vdd',...
    'Length',Lpmos,'Width',W_pmos,'As',2*hdif*W_pmos,'Ad',2*hdif*W_pmos,'Ps',2*hdif+2*W_pmos,'Pd',2*hdif+2*W_pmos);
Netlist{end+1} = ADSaddI_Source(-1,'Ipmosbias','source','0','sink','pmosbias','Idc',abs(DCOP_pmos.Id));
Netlist{end+1} = ADSaddR(-1,'Rload','p','vdd','n','V_PMOS_p1','R',Rl);
Netlist{end+1} = ADSaddShort(-1,'I_PMOS_p1','sink','V_TOTAL_p2','source','V_PMOS_p1','Mode',0,'SaveCurrent','yes');

% total load resistance seen at the output node.
Rltot = 1/(1/Rl+DCOP_pmos.Gds);
gm2 = 1/(Z0*(1+gm1*Rltot)-Rf);

% size the buffer transistor
[W_buffer,DCOP_buffer] = ADSsizeTransistor(NMOS,'Type','NMOS','L',Lmin,'Vov',0.5,'Gm',abs(gm2),'Vs',[],'Vb',0,'Vd',1.8,'Vg',Vout);
Netlist{end+1} = ADSaddMOSFET(-1,'"n_18_mm"','Mbuffer','source','V_BUFF_p2','drain','vdd','gate','V_BUFF_p1','bulk','0',...
    'Length',Lmin,'Width',W_buffer,'As',2*hdif*W_buffer,'Ad',2*hdif*W_buffer,'Ps',2*hdif+2*W_buffer,'Pd',2*hdif+2*W_buffer);
Netlist{end+1} = ADSaddShort(-1,'I_BUFF_p1','sink','V_TOTAL_p2','source','V_BUFF_p1','Mode',0,'SaveCurrent','yes');
Netlist{end+1} = ADSaddShort(-1,'I_BUFF_p2','sink','buff','source','V_BUFF_p2','Mode',0,'SaveCurrent','yes');

% calculate the current through the feedback resistor
Irf = (DCOP_in.Vgs-(Vout-abs(DCOP_buffer.Vgs)))/Rf;
if Irf<0
    error('Irf is smaller than zero')
end
Imirr = abs(DCOP_buffer.Id)+Irf;

% size the bias transistor
[W_bias,DCOP_bias] = ADSsizeTransistor(NMOS,'Type','NMOS','L',Lmin,'Vov',0.3,'Id',Imirr,'Vs',0,'Vb',0,'Vd',Vout-abs(DCOP_buffer.Vgs));
Netlist{end+1} = ADSaddMOSFET(-1,'"n_18_mm"','Mbias','source','0','drain','V_BIAS_p1','gate','bias','bulk','0',...
    'Length',Lmin,'Width',W_buffer,'As',2*hdif*W_buffer,'Ad',2*hdif*W_buffer,'Ps',2*hdif+2*W_buffer,'Pd',2*hdif+2*W_buffer);
Netlist{end+1} = ADSaddMOSFET(-1,'"n_18_mm"','Mbiasbias','source','0','drain','bias','gate','bias','bulk','0',...
    'Length',Lmin,'Width',W_buffer,'As',2*hdif*W_buffer,'Ad',2*hdif*W_buffer,'Ps',2*hdif+2*W_buffer,'Pd',2*hdif+2*W_buffer);
Netlist{end+1} = ADSaddI_Source(-1,'Ibiasbias','source','bias','sink','0','Idc',abs(DCOP_bias.Id));
Netlist{end+1} = ADSaddShort(-1,'I_BIAS_p1','sink','buff','source','V_BIAS_p1','Mode',0,'SaveCurrent','yes');


% size the pullup transistor
[W_pull,DCOP_pull] = ADSsizeTransistor(PMOS,'Type','PMOS','L',Lpmos,'Vov',0.5,'Id',Irf,'Vs',1.8,'Vb',1.8,'Vd',abs(DCOP_in.Vgs));
Netlist{end+1} = ADSaddMOSFET(-1,'"p_18_mm"','Mpullup','source','vdd','drain','V_IN_p1','gate','pullupbias','bulk','vdd',...
    'Length',Lpmos,'Width',W_pull,'As',2*hdif*W_pull,'Ad',2*hdif*W_pull,'Ps',2*hdif+2*W_pull,'Pd',2*hdif+2*W_pull);
Netlist{end+1} = ADSaddMOSFET(-1,'"p_18_mm"','Mpullupbias','source','vdd','drain','pullupbias','gate','pullupbias','bulk','vdd',...
    'Length',Lpmos,'Width',W_pull,'As',2*hdif*W_pull,'Ad',2*hdif*W_pull,'Ps',2*hdif+2*W_pull,'Pd',2*hdif+2*W_pull);
Netlist{end+1} = ADSaddI_Source(-1,'Ipullupbias','source','0','sink','pullupbias','Idc',abs(DCOP_pull.Id));

% add the small signal excitation at the input
Netlist{end+1} = ADSaddShort(-1,'InputBlock','sink','in','source','ins','Mode','1');
Netlist{end+1} = ADSaddR(-1,'Rsource','p','ref','n','ins','R',Z0);
Netlist{end+1} = ADSaddV_Source(-1,'Vac','p','ref','n','0','Vac',1);
sourceLine = length(Netlist);

% perform a DC simulation
Netlist{end+1} = ADSaddDC(-1,'DCsim');
Netlist{end+1} = ADSaddOptions(-1,'Options1','TopologyCheck','yes','TopologyCheckMessages','yes','GiveAllWarnings','yes');
% perform an AC simulation as well
Netlist{end+1} = ADSaddSweepPlan(-1,'ACsweep','start',1,'stop',100e9,'Dec',20);
Netlist{end+1} = ADSaddAC(-1,'ACanal','SweepPlan','ACsweep','SweepVar','freq');

clc

writeTextFile(Netlist,'LNA.net');
simresult = ADSrunSimulation('LNA.net');

AC = simresult.sim2_AC;
DC = simresult.sim1_DC;

figure('name','AC simulation result')
semilogx(AC.freq,db(AC.out))

% figure('name','S11')
% semilogx(AC.freq,db(squeeze(S.S(1,1,:))))

% now perform a distortion contribution analysis on the circuit

% remove the other analysis statements
[Netlist{sourceLine+1:end}] = deal('');
% put a multisine at the input
Netlist{sourceLine} = 'MS:MSsource ref 0 fmin=25e6 fmax=1000e6 gridtype="odd" amp=0.001 Rout=0';
% add the DCA statement
Netlist{end+1} = 'DCA:DCA1 port="2" numberOfRealisations=50 display="res" useBLA=no ticklerDistance=1000 outOfBandFactor=2 oversample=6 showSteps=yes';

writeTextFile(Netlist,'LNA.net');

DCA_v3('LNA.net');

