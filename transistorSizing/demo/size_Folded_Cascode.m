% In this script, a folded cascode op-amp is sized using the ADSsizeTransistor function
clc
close all
clear all


NMOS = readTextFile('NMOS_3.3V_basicNetlist.net');
PMOS = readTextFile('PMOS_3.3V_basicNetlist.net');

Netlist{1} = '';
Netlist{end+1}='#define TT TT';
Netlist{end+1}='#ifndef inc_Z__UMC_180_models_umc18config_net';
Netlist{end+1}='#define inc_Z__UMC_180_models_umc18config_net inc_Z__UMC_180_models_umc18config_net';
Netlist{end+1}='#include "Z:\UMC_180\models\umc18config.net"';
Netlist{end+1}='#endif';
Netlist{end+1}='#undef TT';
Netlist{end+1}='';

% "p_33_mm":Mload  drain gate source bulk Length=%s Width=%s
% "n_33_mm":Mtest  drain gate source bulk Length=%s Width=%s

Netlist{end+1} = ADSaddV_Source(-1,'Vsupply','p','vdd','n','0','Type','V_DC','Vdc',3.3);


% specifications
Cl = 10e-12;
GBW = 100e6;

Netlist{end+1} = ADSaddC(-1,'Cl','p','out','n','0','C',Cl);

Lmin = 0.24e-6;

gamma = 4;

fnd = gamma*GBW;

% approximate the location of the non-dominant pole to get an estimate of gm2
gm2 = fnd * 2 * pi * Cl;

Lout = 0.3e-6;

% size the ouput transistor
[W_out,DCOP_out] = ADSsizeTransistor(NMOS,'Gm',gm2,'L',Lout,'Vov',0.2,'Vd',1.65);

Netlist{end+1} = sprintf('"n_33_mm":Mout out shift 0 0 Length=%s Width=%s',eng(Lout),eng(W_out));

% size the PMOS load
[W_load,DCOP_load] = ADSsizeTransistor(PMOS,'Id',DCOP_out.Id,'L',Lout,'Vov',0.2,'Type','PMOS','Vb',3.3,'Vs',3.3,'Vd',1.65);

Netlist{end+1} = sprintf('"p_33_mm":Mload  out loadbias vdd vdd Length=%s Width=%s ',eng(Lout),eng(W_load));
Netlist{end+1} = ADSaddV_Source(-1,'Vbiasload','p','loadbias','n','0','Type','V_DC','Vdc',3.3-abs(DCOP_load.Vgs));

% determine the size of the compensation capacitor
Cc = Cl/5;

Netlist{end+1} = ADSaddC(-1,'Cc','p','out','n','int','C',Cc);

gm1 = GBW*2*pi*Cc;


spelingTop = 0.3;
spelingBot = 0.5;

Lin = 1e-6;

% size the input transistors
[W_in,DCOP_in] =  ADSsizeTransistor(PMOS,'Gm',gm1,'L',Lin,'Vov',0.2,'Type','PMOS','Vb',2.55,'Vs',2.55,'Vd',spelingBot);

Netlist{end+1} = sprintf('"p_33_mm":Minm  botm inm top top Length=%s Width=%s ',eng(Lin),eng(W_in));
Netlist{end+1} = sprintf('"p_33_mm":Minp  botp inp top top Length=%s Width=%s ',eng(Lin),eng(W_in));

Iin = abs(DCOP_in.Id);

% size the top transistor
[W_top,DCOP_top] = ADSsizeTransistor(PMOS,'Id',Iin,'L',1e-6,'Vov',0.2,'Type','PMOS','Vb',3.3,'Vs',3.3,'Vd',2.55);

Netlist{end+1} = sprintf('"p_33_mm":Mtop  top topbias vdd vdd Length=%s Width=%s ',eng(Lin),eng(W_top));
Netlist{end+1} = ADSaddV_Source(-1,'Vtopbias','p','topbias','n','0','Type','V_DC','Vdc',3.3-abs(DCOP_top.Vgs));


% assume that the same current runs through the cascodes as through the input transistors.
Icasc = Iin;
Lcasc = 2e-6;

% size the bottom transistor
[W_bot,DCOP_bot] = ADSsizeTransistor(NMOS,'Id',Icasc+Iin,'Vov',0.2,'L',Lcasc,'Vd',spelingBot);

Netlist{end+1} = sprintf('"n_33_mm":Mbotp botp botbias 0 0 Length=%s Width=%s',eng(Lcasc),eng(W_bot));
Netlist{end+1} = sprintf('"n_33_mm":Mbotm botm botbias 0 0 Length=%s Width=%s',eng(Lcasc),eng(W_bot));
Netlist{end+1} = ADSaddV_Source(-1,'Vbotbias','p','botbias','n','0','Type','V_DC','Vdc',abs(DCOP_bot.Vgs));

% size the top PMOS transistor
[W_mirr,DCOP_mirr] = ADSsizeTransistor(PMOS,'Id',Icasc,'Vov',0.2,'L',Lcasc,'Type','PMOS','Vb',3.3,'Vs',3.3,'Vd',3.3-spelingTop);

Netlist{end+1} = sprintf('"p_33_mm":Mmirrp casctopp mirr vdd vdd Length=%s Width=%s ',eng(Lcasc),eng(W_mirr));
Netlist{end+1} = sprintf('"p_33_mm":Mmirrm casctopm mirr vdd vdd Length=%s Width=%s ',eng(Lcasc),eng(W_mirr));

% now we know the output voltage of the output stage, we can size the last transistors
Vint = 3.3 - abs(DCOP_mirr.Vgs);

% size the top cascode transistor
[W_casctop,DCOP_casctop] = ADSsizeTransistor(PMOS,'Id',Icasc,'Vov',0.2,'L',Lcasc,'Type','PMOS','Vb',3.3,'Vs',3.3-spelingTop,'Vd',Vint);
Netlist{end+1} = sprintf('"p_33_mm":Mcasctopp int  casctopbias casctopp vdd Length=%s Width=%s ',eng(Lcasc),eng(W_casctop));
Netlist{end+1} = sprintf('"p_33_mm":Mcasctopm mirr casctopbias casctopm vdd Length=%s Width=%s ',eng(Lcasc),eng(W_casctop));
Netlist{end+1} = ADSaddV_Source(-1,'Vcasctopbias','p','casctopbias','n','0','Type','V_DC','Vdc',3.3-spelingTop-abs(DCOP_casctop.Vgs));

[W_cascbot,DCOP_cascbot] = ADSsizeTransistor(NMOS,'Id',Icasc,'Vov',0.2,'L',Lcasc,'Type','NMOS','Vb',0,'Vs',spelingBot,'Vd',Vint);
Netlist{end+1} = sprintf('"n_33_mm":Mcascbotp int cascbotbias botp 0 Length=%s Width=%s',eng(Lcasc),eng(W_cascbot));
Netlist{end+1} = sprintf('"n_33_mm":Mcascbotm mirr cascbotbias botm 0 Length=%s Width=%s',eng(Lcasc),eng(W_cascbot));
Netlist{end+1} = ADSaddV_Source(-1,'Vcascbotbias','p','cascbotbias','n','0','Type','V_DC','Vdc',spelingBot+abs(DCOP_cascbot.Vgs));

Ishift = Icasc;

% the level shifter remains
[W_shift,DCOP_shift] = ADSsizeTransistor(NMOS,'Type','NMOS','Id',Ishift,'L',Lout,'Vg',Vint,'Vb',0,'Vd',3.3,'Vs',DCOP_out.Vgs);

Netlist{end+1} = sprintf('"n_33_mm":Mshift vdd int shift 0 Length=%s Width=%s',eng(Lout),eng(W_shift));

% and its bias transistor
[W_shiftbias,DCOP_shiftbias] = ADSsizeTransistor(NMOS,'Type','NMOS','Id',Ishift,'L',Lout,'Vov',0.2,'Vb',0,'Vd',DCOP_out.Vgs,'Vs',0);

Netlist{end+1} = sprintf('"n_33_mm":Mshiftbias shift shiftbias 0 0 Length=%s Width=%s',eng(Lout),eng(W_shiftbias));
Netlist{end+1} = ADSaddV_Source(-1,'Vshiftbias','p','shiftbias','n','0','Type','V_DC','Vdc',abs(DCOP_shiftbias.Vgs));

% add the DC source at the input
mode = 'closedloop';
switch mode
    case 'openloop'
        Netlist{end+1} = ADSaddV_Source(-1,'Vsgnd','p','sgnd','n','0','Type','V_DC','Vdc',1.65);
        Netlist{end+1} = ADSaddV_Source(-1,'Vacp','p','inp','n','sgnd','Vac',0.5);
        Netlist{end+1} = ADSaddV_Source(-1,'Vacm','p','inm','n','sgnd','Vac',-0.5);
    case 'closedloop'
        Netlist{end+1} = ADSaddV_Source(-1,'Vsgnd','p','sgnd','n','0','Type','V_DC','Vdc',1.65);
        Netlist{end+1} = ADSaddR(-1,'Rin','p','in','n','inm','R',1000);
        Netlist{end+1} = ADSaddR(-1,'Rfb','p','out','n','inm','R',5000);
        Netlist{end+1} = ADSaddV_Source(-1,'Vacm','p','in','n','sgnd','Vac',1);
        Netlist{end+1} = ADSaddV_Source(-1,'Vp','p','inp','n','0','Type','V_DC','Vdc',1.65);
end



% run an AC simulation on the netlist
Netlist{end+1} = ADSaddSweepPlan(-1,'ACsweep','start',1,'stop',1e9,'Dec',20);
Netlist{end+1} = ADSaddAC(-1,'ACanal','SweepPlan','ACsweep','SweepVar','freq');
Netlist{end+1} = ADSaddOptions(-1,'Options1','TopologyCheck','yes','TopologyCheckMessages','yes','GiveAllWarnings','yes');
Netlist{end+1} = ADSaddDC(-1,'DCanal');

writeTextFile(Netlist,'foldedcasc.net');

simres = ADSrunSimulation('foldedcasc.net');
AC = simres.sim1_AC;
DC = simres.sim2_DC;

figure('name','results of the AC simulatopm')
subplot(211)
semilogx(AC.freq,db(AC.out./AC.inm))
hold on
semilogx(AC.freq,db(AC.int./AC.inm),'r')
semilogx(AC.freq,db(AC.out./AC.int),'k')
semilogx(AC.freq,db(AC.shift./AC.int),'k')
grid on
legend({'out/in','int/in','out/int','shift/int'});
subplot(212)
semilogx(AC.freq,angle(AC.out./AC.inm)/pi*180)
hold on
semilogx(AC.freq,angle(AC.int./AC.inm)/pi*180,'r')
semilogx(AC.freq,angle(AC.out./AC.int)/pi*180,'k')
semilogx(AC.freq,angle(AC.shift./AC.int)/pi*180,'k')
grid on
legend({'out/in','int/in','out/int','shift/int'});
