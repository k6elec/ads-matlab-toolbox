clc
close all
clear all

cd(fileparts(which('size_Miller_Opamp_test.m')))


Cl = 1e-12;

% the default netlists that contain the correct transistors
NMOS = readTextFile('NMOS_3.3V_basicNetlist.net');
PMOS = readTextFile('PMOS_3.3V_basicNetlist.net');

keyboard
Netlist = size_Miller_Opamp('NMOS',NMOS,'PMOS',PMOS,'addCurrentSources',true,'subcircuit',false,...
    'alpha',4,'Lout_N',1e-6,'Lout_P',1e-6,'Cload',Cl);

Netlist{end+1}='';
Netlist{end+1}='#define TT TT';
Netlist{end+1}='#ifndef inc_V__DesignKits_UMC180_ADS_models_umc18config_net';
Netlist{end+1}='#define inc_V__DesignKits_UMC180_ADS_models_umc18config_net inc__V__DesignKits_UMC180_ADS_models_umc18config_net';
Netlist{end+1}='#include "V:\DesignKits\UMC180_ADS\models\umc18config.net"';
Netlist{end+1}='#endif';
Netlist{end+1}='#undef TT';
Netlist{end+1}='';

% add the load capacitor
Netlist{end+1}=ADSaddC(-1,'Cl','p','out','n','0','C',Cl);

% add the feedback network
Netlist{end+1}=ADSaddR(-1,'Rfb','p','out','n','inm','R',50e3);
Netlist{end+1}=ADSaddR(-1,'Rin','p','in','n','inm','R',10e3);
% add the singal groud source
Netlist{end+1}=ADSaddV_Source(-1,'SGND','p','inp','n','0','Vdc',1.65,'Vac',0);



% add a small-signal excitation to test the response of the designed op-amp
Netlist{end+1}=ADSaddV_Source(-1,'AC','p','in','n','inp','Vdc',0,'Vac',1);
% and add the simulation statements
Netlist{end+1} = ADSaddSweepPlan(-1,'ACsweep','start',1,'stop',1e9,'Dec',20);
Netlist{end+1} = ADSaddAC(-1,'ACanal','SweepPlan','ACsweep','SweepVar','freq');
Netlist{end+1} = ADSaddOptions(-1,'Options1','TopologyCheck','yes','TopologyCheckMessages','yes','GiveAllWarnings','yes');
Netlist{end+1} = ADSaddDC(-1,'DCanal');

writeTextFile(Netlist,'MillerOpamp.net');

clc

simres = ADSrunSimulation('MillerOpamp.net');
AC = simres.sim1_AC;
DC = simres.sim2_DC;

figure
subplot(211)
semilogx(AC.freq,db(AC.out./AC.inm));
grid on
subplot(212)
semilogx(AC.freq,angle(AC.out./AC.inm)/2/pi*360);
grid on
