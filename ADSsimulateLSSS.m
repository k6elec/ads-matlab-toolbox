function [res,spec,rawSimData,superRawSimData] = ADSsimulateLSSS(netlist,MSdef,varargin)
% ADSSIMULATELSSS performs a Large-Signal-Small-Signal simulation in ADS performs some basic processing on the results
%
%    [res,spec,rawSimData,superRawSimData] =  ADSSIMULATELSSS(netlist,MSdef)
%    [res,spec,rawSimData,superRawSimData] =  ADSSIMULATELSSS(netlist,MSdef,'ParamName',ParamValue)
%
% The function adds small-singal source(s) to the netlist and
% performs a Large-Signal Small-Signal simualtion around the non-linear
% operating point set by the multisine provided in MSdef.
% The function overwrites two parameters in the harmonic balance
% settings: The SamanskiiConstant and GuardThres are both set to zero. This
% is done to avoid truncation in the jacobian, which leads to errors in the
% small-singal FRFs. The Samanskii Constant allows skipping of the
% calculation of the jacobian in some steps. Setting it to zero forces
% calculation of the jacobian in every iteration. The guard threshold
% is a factor that specifies when elements in the jacobian can be
% set to zero. By setting GuardThresh to zero, nor rounding occurs in the
% jacobian.
% Like ADSsimulateMSwaves, this function as a layer on top of
% ADSsimulateMS. If you want, you can pass extra arguments for 
% ADSsimulateMS to this function. The default number of
% realisations is set to 1 though.
%
% Required Inputs:
%   netlist  Default:  CheckFunction: @(x) ischar(x)|iscellstr(x)
%     filename of the netlist that will be simulated.
%   MSdef  Default:  CheckFunction: @isstruct
%     Describes the Multisine that sets the large sigal operating point. The
%     MSdef struct is passed on to the ADSsimulateMS function, so you need the
%     same fields as for that one (MSdef.MSnode indicates the node the
%     multisine is connected to). Create your multisine with the MScreate
%     function.
% Parameter-Value pairs:
%   LSSSNode  Default: 'in' CheckFunction: @(x) ischar(x)|iscell(x)
%     node(s) to which the LSSS source(s) is connected. You can pass a string
%     with the nodename, or a cell array of strings if you want multiple nodes
%     excited. If you want to connect a small signal source between nodes, pass
%     a cell array of 2 strings for that source (ex. 1 source between node 'a'
%     and 'b' gives {{'a','b'}}).
%     If you provide multiple nodes, the sources are excited one by one
%   Rsource  Default: Inf CheckFunction: @isscalar
%     output impedance of the small-signal source. Default is a current source.
%     If you use a voltage source, or something with a finite output impedance,
%     keep in mind that the DC and Large-signal operating point of the circuit
%     will be infulenced.
%   Start  Default: 1 CheckFunction: @isscalar
%     start frequency of the LSSS sweep
%   Stop  Default: 1 CheckFunction: @isscalar
%     stop frequency of the LSSS sweep
%   Step  Default: 1 CheckFunction: @(x) isscalar(x)||isempty(x)
%     frequency step of the LSSS sweep
%   Dec  Default: [] CheckFunction: @(x) isscalar(x)||isempty(x)
%     Points per decade in the LSSS sweep. if this parameter is set, a
%     logarithmic sweep is performed
%   DiskGrid  Default: false CheckFunction: @islogical
%     set DiscGrid to true if you want a special frequency grid optimised for
%     stability analysis. The 'Stop' parameter controls the maximum frequency,
%     while the DiskGridPoints controls the amount of points
%   DiskGridPoints  Default: 10 CheckFunction: @isnatural
%     power of two that determined the amount of points in the frequency grid
%     for stability analysis. The analysis will use 2^DiscGridPoints in its
%     fourier transform
%   SweepPlan  Default: '' CheckFunction: @ischar
%     you can provide the name of a custom sweep plan to the function. This
%     sweepplan will override all other sweep parameters provided to the function
%   Krylov  Default: false CheckFunction: @islogical
%     indicates whether the Krylov method can be used in the harmonic balance
%     simulation. By default this is turned off, because it can give weird
%     results. But the robust method in HB is way more slow.
%   GuardThreshZero  Default: true CheckFunction: @islogical
%     if this paramter is true, the guard threshold is set to zero. This will
%     disable truncation in the jacobian matrix
%   Waves  Default: false CheckFunction: @islogical
%     the waves boolean indicates whether ADSsimulateMS or ADSsimulateMSwaves
%     should be called internally. If you set this to true, all the A and B waves in
%     the circuit are calculated at the location of each current source.
%   OscPort  Default: [] CheckFunction: @ischar
%     This points toward the name of the oscport in the circuit. If you provide
%     this parameter, an oscillator analysis will be performed on the circuit
%     and the LSSS simulation will be run around the obtained orbit
% 
% Outputs: 
%   res Type: vector of structs
%     result of the large-signal small-signal simulation. This struct
%     contains a NxExF matrix for each named signal in the netlist, where N is
%     the amount of small-signal sources, E the amount of points in the
%     small-signal sweep and F is the amount of output tones of the LSSS
%     simulation. This amount of tones is 2*O+1 where O is the amount of
%     frequencies in the harmonic balance simulation. If multiple
%     realisations of the multisine are simulated, the result is a vector of
%     structs with its length equal to the amount of realisations.
%     The base frequency of variation, fsys, is added to the result struct as well
%   spec Type: struct 
%     Classic result of the harmonic balance simulation in the
%     ADSsimulateMS function. This is the spectrum that has been used in the
%     linearisation for the small-signal simulation
%   rawSimData Type: cell array of structs
%     raw simulation data from ADS, loaded with ADSimportSimData
%   superRawSimData Type: 
%     
%
% 
% See Also: ADSSIMULATEMS


% every good function starts with an inputParser statement
p = inputParser();
p.KeepUnmatched = true;
p.FunctionName= 'ADSsimulateLSSS';

% filename of the netlist that will be simulated.
p.addRequired('netlist',@(x) ischar(x)|iscellstr(x));

% Describes the Multisine that sets the large sigal operating point. The
% MSdef struct is passed on to the ADSsimulateMS function, so you need the
% same fields as for that one (MSdef.MSnode indicates the node the
% multisine is connected to). Create your multisine with the MScreate 
% function.
p.addRequired('MSdef',@isstruct);

% node(s) to which the LSSS source(s) is connected. You can pass a string
% with the nodename, or a cell array of strings if you want multiple nodes
% excited. If you want to connect a small signal source between nodes, pass
% a cell array of 2 strings for that source (ex. 1 source between node 'a'
% and 'b' gives {{'a','b'}}).
% If you provide multiple nodes, the sources are excited one by one
p.addParamValue('LSSSNode','in',@(x) ischar(x)|iscell(x));

% output impedance of the small-signal source. Default is a current source.
% If you use a voltage source, or something with a finite output impedance,
% keep in mind that the DC and Large-signal operating point of the circuit
% will be infulenced.
p.addParamValue('Rsource',Inf,@isscalar);

% start frequency of the LSSS sweep
p.addParamValue('Start',1,@isscalar);

% stop frequency of the LSSS sweep
p.addParamValue('Stop',1,@isscalar);

% frequency step of the LSSS sweep
p.addParamValue('Step',1,@(x) isscalar(x)||isempty(x));

% Points per decade in the LSSS sweep. if this parameter is set, a
% logarithmic sweep is performed 
p.addParamValue('Dec',[],@(x) isscalar(x)||isempty(x));

% set DiscGrid to true if you want a special frequency grid optimised for
% stability analysis. The 'Stop' parameter controls the maximum frequency,
% while the DiskGridPoints controls the amount of points
p.addParamValue('DiskGrid',false,@islogical);

% power of two that determined the amount of points in the frequency grid
% for stability analysis. The analysis will use 2^DiscGridPoints in its
% fourier transform
p.addParamValue('DiskGridPoints',10,@isnatural);

% you can provide the name of a custom sweep plan to the function. This
% sweepplan will override all other sweep parameters provided to the function
p.addParamValue('SweepPlan','',@ischar);

% indicates whether the Krylov method can be used in the harmonic balance
% simulation. By default this is turned off, because it can give weird
% results. But the robust method in HB is way more slow.
p.addParamValue('Krylov',false,@islogical);

% if this paramter is true, the guard threshold is set to zero. This will
% disable truncation in the jacobian matrix
p.addParamValue('GuardThreshZero',true,@islogical);

% the waves boolean indicates whether ADSsimulateMS or ADSsimulateMSwaves
% should be called internally. If you set this to true, all the A and B waves in
% the circuit are calculated at the location of each current source.
p.addParamValue('Waves',false,@islogical);

% This points toward the name of the oscport in the circuit. If you provide
% this parameter, an oscillator analysis will be performed on the circuit
% and the LSSS simulation will be run around the obtained orbit
p.addParamValue('OscPort',[],@ischar);

p.parse(netlist,MSdef,varargin{:});
args = p.Results();
extra_args = p.Unmatched();
clear netlist MSdef varargin p

%% perform some more checks on the input

% put the netlist in a cell array
if ~iscell(args.netlist)
    args.netlist=readTextFile(args.netlist);
end

% if LSSSNode is a string, put it in a cell array
if ~iscell(args.LSSSNode)
    args.LSSSNode = {args.LSSSNode};
end
% go through the cell array of nodes to make all the connections correctly
for ii=1:length(args.LSSSNode)
    if ischar(args.LSSSNode{ii})
        args.LSSSNode{ii} = {args.LSSSNode{ii},'0'};
    else
        if iscell(args.LSSSNode{ii})
            if length(args.LSSSNode{ii})==1
                args.LSSSNode{ii} = {args.LSSSNode{ii}{1},'0'};
                keyboard
            elseif length(args.LSSSNode)>2
                error('you can only provide two nodes for a connection of a small signal source')
            end
        end
    end
end


%% add the small signal source(s) to the netlist

numTicklers = length(args.LSSSNode);

if args.DiskGrid
    % if the special DiskGrid is used, the frequency sweep will be
    % performed outside of the LSSS simulation, so we sweep the FreqSweep,
    % instead of the LSSS itself
    args.netlist{end+1} = ADSaddParamSweep(-1,'TickleSweep','SimInstanceName','FreqSweep','SweepVar','SS_ampindex','SweepPlan','TickleSweepPlan');
else
    % add the correct variables to the netist to be able to perform the sweep of the amplitudes of the different ports
    args.netlist{end+1} = ADSaddParamSweep(-1,'TickleSweep','SimInstanceName','HARMONICBALANCE_sim','SweepVar','SS_ampindex','SweepPlan','TickleSweepPlan');
end
args.netlist{end+1} = ADSaddSweepPlan(-1,'TickleSweepPlan','Start',1,'Stop',numTicklers,'Step',1);
% SS_ampindex is the one that will be swept
args.netlist{end+1} = sprintf('SS_ampindex=1');
% the diagonal excitation matrix is passed in the SS_amplitude list
args.netlist{end+1} = sprintf('SS_amplitude=%s',generateAmpList(numTicklers));

for ii=1:numTicklers
    if args.Rsource==Inf
        %
        %    o------Small-Signal current source----o
        %      ^                 ^              ^
        %      |                 |              |
        % LSSSNode{X}{1}    LSSSSourceX       LSSSNode{X}{2}
        
        % add the small signal current source
        args.netlist{end+1} = sprintf('I_Source:LSSSSource%d  %s %s Type="I_1Tone" I[1]=0 Freq[1]=0  I_USB[1]=SS_amplitude[%d*(int(SS_ampindex)-1)+%d]',ii,args.LSSSNode{ii}{2},args.LSSSNode{ii}{1},numTicklers,ii);
    else
        %          R_LSSSX   
        %             |            
        %             v            
        %    o-----/\/\/\-----------Small-Signal voltage source----o
        %      ^  Rsource       ^                 ^              ^
        %      |                |                 |              |
        % LSSSNode{X}{1}     N__1000+X       LSSSSourceX      LSSSNode{X}{2}
        
        % add a zero ohm resistor between the stage node and the ticlker reference probe to prevent collision of renamed nodes
        args.netlist{end+1} = sprintf('R:R_LSSS%d  N__%d %s R=%s',ii,1000+ii,args.LSSSNode{ii}{1},eng(args.Rsource));
        % add the small signal current source
        args.netlist{end+1} = sprintf('V_Source:LSSSSource%d  %s N__%d Type="V_1Tone" V[1]=0 Freq[1]=0  V_USB[1]=SS_amplitude[%d*(int(SS_ampindex)-1)+%d]',ii,args.LSSSNode{ii}{2},1000+ii,numTicklers,ii);
    end
end

%% add the LSSS sweep to the netlist
% if there's a custom sweepPlan provided, don't add this to the netlist
if isempty(args.SweepPlan)
    if args.DiskGrid
        % SS_N is the power of two that indicates the amount of points on the unit circle.
        args.netlist{end+1} = sprintf('SS_N=%s',num2str(args.DiskGridPoints));
        % SS_Fmax is the maximum frequency in the simulation
        args.netlist{end+1} = sprintf('SS_Fmax=%s',eng(args.Stop));
        % SS_Norm is the normalisation used after theta is translated to theimaginary axis
        args.netlist{end+1} = 'SS_Norm=SS_Fmax/(1+sqrt(2))';
        % SS_ind is the index that will be swept
        args.netlist{end+1} = 'SS_ind=1';
        % SS_theta are the wanted angles on the circle. They go from pi to -pi/4
        args.netlist{end+1} = 'SS_theta=2*pi*SS_ind/(2^SS_N)';
        % SS_Z is the complex number related to the corresponding theta
        args.netlist{end+1} = 'SS_Z=exp(j*SS_theta)';
        % SS_Freq is the frequency at which the simulation will be performed
        args.netlist{end+1} = 'SS_Freq=imag((SS_Z+1)/(SS_Z-1))*SS_Norm';
        % The custom frequency sweep changes the index
        args.netlist{end+1} = 'ParamSweep:FreqSweep SimInstanceName[1]="HARMONICBALANCE_sim" SweepVar="SS_ind" SweepPlan="FreqSweepPlan"';
        % the index is swept over values that correspond to the pi -> -pi/4 interval
        args.netlist{end+1} = 'SweepPlan:FreqSweepPlan Start=(2^SS_N/2) Stop=(2^SS_N-2^SS_N/8) Step=1';
    else
        if isempty(args.Dec)
            % linear sweep
            args.netlist{end+1} = ADSaddSweepPlan(-1,'LSSSsweep','Start',eng(args.Start,3,20),'Stop',eng(args.Stop,3,20),'Step',eng(args.Step,3,20));
        else
            % logarithmic sweep
            args.netlist{end+1} = ADSaddSweepPlan(-1,'LSSSsweep','Start',eng(args.Start,3,20),'Stop',eng(args.Stop,3,20),'Dec',args.Dec);
        end
    end
end

%% Calculate the settings for the harmonic balance simulator or edit them to make sure the LSSS simulation returns reliable results

% set the simulator to be HB by default
extra_args.Simulator = 'HB';

% set the default number of realisations to be 1
if ~isfield(extra_args,'numberOfRealisations') 
    extra_args.numberOfRealisations = 1;
end

% If the simsettings are not provided, either use the provided HB settings, or calculate them
if ~isfield(extra_args,'SimSettingsStruct')
    if ~isfield(extra_args,'HB_freq')
        if ~isfield(extra_args,'oversample')
            extra_args.SimSettingsStruct = calculateHBSettings(args.MSdef(1),10);
        else
            extra_args.SimSettingsStruct = calculateHBSettings(args.MSdef(1),extra_args.oversample);
        end
    else
        % if HB_freq and HB_order are provided, use those settings
        extra_args.SimSettingsStruct.Freq = extra_args.HB_freq;
        extra_args.SimSettingsStruct.Order = extra_args.HB_order;
        extra_args.SimSettingsStruct.MaxOrder = extra_args.SimSettingsStruct.Order(1);
        extra_args = rmfield(extra_args,{'HB_freq','HB_order'});
    end
end

% add the specific simulation settings for the LSSS simulation
if isempty(args.SweepPlan)
    if args.DiskGrid
        extra_args.SimSettingsStruct.SS_Freq='SS_Freq';
    else
        extra_args.SimSettingsStruct.SS_Plan='LSSSsweep';
    end
else
    extra_args.SimSettingsStruct.SS_Plan=args.SweepPlan;
end


if args.Krylov
    extra_args.SimSettingsStruct.UseKrylov=1;% use the Krylov solver
else
    extra_args.SimSettingsStruct.UseKrylov=0;% use the Direct solver
end
extra_args.SimSettingsStruct.SamanskiiConstant=0; % force the jacobian to be calculated at each step
if args.GuardThreshZero
    extra_args.SimSettingsStruct.GuardThresh=0;       % no truncation in the jacobian
end

% if the oscPort parameter is passed, run the thing in oscillator mode
if ~isempty(args.OscPort)
%     extra_args.SimSettingsStruct.OscMode='yes';
    extra_args.SimSettingsStruct.OscPortName=['"' args.OscPort '"'];
end


%% perform the LSSS simulation
if args.Waves
    [spec,waves_spec,rawSimData,superRawSimData] = ADSsimulateMSwaves(args.netlist,args.MSdef,extra_args);
else
    [spec,rawSimData,superRawSimData] = ADSsimulateMS(args.netlist,args.MSdef,extra_args);
end

%% process the results

% if there's only one realisation, the result is a struct. Put it in a cell array
if isstruct(rawSimData)
    rawSimData = {rawSimData};
end

% go backwards in the result struct to avoid preallocation error
for ii=length(rawSimData):-1:1
    % get the small signal excitation frequencies out of the result struct
    if ii==1
        if args.DiskGrid
            % if the DiskGrid parameter is true, the frequency axis generated by ADS is
            % rubbish, regenerate it by calculating the same formulas as those that
            % were passed to the netlist earlier
            SS_N=args.DiskGridPoints;
            SS_Fmax=args.Stop;
            SS_Norm=SS_Fmax/(1+sqrt(2));
            SS_theta=2*pi*rawSimData{ii}.sim2_LSSS.sweepvar_SS_ind/(2^SS_N);
            SS_Z=exp(1i*SS_theta).';
            ssfreq = imag((SS_Z+1)./(SS_Z-1))*SS_Norm;
        else
            ssfreq = rawSimData{ii}.sim2_LSSS.sweepvar_ssfreq;
        end
    end
    
    % remove the unneeded fields from the simulation results
    if args.DiskGrid     
        rawSimData{ii}.sim2_LSSS = rmfield(rawSimData{ii}.sim2_LSSS,{'sweepvar_SS_ampindex','sweepvar_SS_ind'});
    else
        rawSimData{ii}.sim2_LSSS = rmfield(rawSimData{ii}.sim2_LSSS,{'sweepvar_SS_ampindex','sweepvar_ssfreq'});
    end
    
    % the LSSS results are in the second place of the simulation
    res(ii) = rawSimData{ii}.sim2_LSSS;
    
    % permute the results, I want the sweep of the tone to come first
    fields = fieldnames(res(ii));
    for ff=1:length(fields)
        res(ii).(fields{ff}) = permute(res(ii).(fields{ff}),[2 1 3]);
    end
end

% add fsys and ssfreq to the result struct
[res.fsys] = deal(min(spec.freq(spec.freq~=0)));
[res.ssfreq] = deal(ssfreq);

% clean up the spec result a little. If multiple nodes are present in the
% LSSS simulation, the HB simulation is swept and the results of spec are
% returned mulltiple times. This is not necesssary.
if isfield(spec,'sweepvar_SS_ampindex'); spec = rmfield(spec,'sweepvar_SS_ampindex');end;
if isfield(spec,'sweepvar_ind'); spec = rmfield(spec,'sweepvar_ind');end;
fields = fieldnames(spec);
for ii=1:length(fields)
    if ~strcmpi(fields{ii},'freq')
        spec.(fields{ii}) = spec.(fields{ii})(:,1,:);
    end
end

end 

function res=generateAmpList(n)
% the excitation matrix is a diagonal matrix
mat = eye(n);
% start the list statement
res = 'list(';
% add the ones and zeros of the excitation matrix
for ii=1:n
    for jj=1:n
        res = [res num2str(mat(ii,jj)) ','];%#ok
    end
end
% there's a comma too much, throw it away
res = res(1:end-1);
% add the closing bracket
res = [res ')'];
end

% @generateFunctionHelp 

% @Tagline performs a Large-Signal-Small-Signal simulation in ADS performs some basic processing on the results
% @Description The function adds small-singal source(s) to the netlist and
% @Description performs a Large-Signal Small-Signal simualtion around the non-linear
% @Description operating point set by the multisine provided in MSdef.

% @Description The function overwrites two parameters in the harmonic balance
% @Description settings: The SamanskiiConstant and GuardThres are both set to zero. This
% @Description is done to avoid truncation in the jacobian, which leads to errors in the
% @Description small-singal FRFs. The Samanskii Constant allows skipping of the
% @Description calculation of the jacobian in some steps. Setting it to zero forces
% @Description calculation of the jacobian in every iteration. The guard threshold
% @Description is a factor that specifies when elements in the jacobian can be
% @Description set to zero. By setting GuardThresh to zero, nor rounding occurs in the
% @Description jacobian.

% @Description Like ADSsimulateMSwaves, this function as a layer on top of
% @Description ADSsimulateMS. If you want, you can pass extra arguments for 
% @Description ADSsimulateMS to this function. The default number of
% @Description realisations is set to 1 though.
%
% @SeeAlso ADSsimulateMS

% @Outputs{1}.Type vector of structs
% @Outputs{1}.Description{1} result of the large-signal small-signal simulation. This struct
% @Outputs{1}.Description{2} contains a NxExF matrix for each named signal in the netlist, where N is
% @Outputs{1}.Description{3} the amount of small-signal sources, E the amount of points in the
% @Outputs{1}.Description{4} small-signal sweep and F is the amount of output tones of the LSSS
% @Outputs{1}.Description{5} simulation. This amount of tones is 2*O+1 where O is the amount of
% @Outputs{1}.Description{6} frequencies in the harmonic balance simulation. If multiple
% @Outputs{1}.Description{7} realisations of the multisine are simulated, the result is a vector of
% @Outputs{1}.Description{8} structs with its length equal to the amount of realisations.
% @Outputs{1}.Description{9} The base frequency of variation, fsys, is added to the result struct as well

% @Outputs{2}.Type struct 
% @Outputs{2}.Description{1} Classic result of the harmonic balance simulation in the
% @Outputs{2}.Description{2} ADSsimulateMS function. This is the spectrum that has been used in the
% @Outputs{2}.Description{3} linearisation for the small-signal simulation

% @Outputs{3}.Type cell array of structs
% @Outputs{3}.Description raw simulation data from ADS, loaded with ADSimportSimData