function [rms,errorTransient] = checkTransients(rawSimData,periods,varargin)
% CHECKTRANSIENTS checks transient energy in the simulation data. 
%
%    [rms,errorTransient] =  CHECKTRANSIENTS(rawSimData,periods)
%    [rms,errorTransient] =  CHECKTRANSIENTS(rawSimData,periods,'ParamName',ParamValue)
%
% Calculates the RMS error and outputs a struct containing the error at each node.
%
% Required Inputs:
%   rawSimData  Default:  CheckFunction: @(x) iscell(x) || isstruct(x)
%     Contains the raw simulation data as loaded by the ADSimportSimData
%     function. It is a vector of length M
%   periods  Default:  CheckFunction: @(x) isnumeric(x) && isscalar(x)
%     number of periods simulated in every TRAN simulation. All periods are
%     passed to the output, so it's best to use only the last one, because it
%     will contain the best steady-state response.
% Parameter-Value pairs:
%   fieldfilter  Default: [] CheckFunction: @iscellstr
%     cell of the fieldnames of the struct that should be converted,
%     when this cell array is empty all fields will be processed.
% 
% Outputs: 
%   rms Type: struct
%     Contains the RMS-error of the periods of the rawSimData
%     with the final period as the reference. This struct has the same
%     structure as the rawSimdata.
%   errorTransient Type: struct
%     Contains the difference (in time-domain) between the last and
%     previous periods of the rawSimData.
%     This struct has the same structure as the rawSimdata.
%
% 
% See Also: 

%% parse the input

p = inputParserEgon();
p.FunctionName = 'checkTransients';

% Contains the raw simulation data as loaded by the ADSimportSimData
% function. It is a vector of length M
p.addRequired('rawSimData',@(x) iscell(x) || isstruct(x));

% number of periods simulated in every TRAN simulation. All periods are
% passed to the output, so it's best to use only the last one, because it
% will contain the best steady-state response.
p.addRequired('periods',@(x) isnumeric(x) && isscalar(x));

% cell of the fieldnames of the struct that should be converted, 
% when this cell array is empty all fields will be processed.
p.addParamValue('fieldfilter',[],@iscellstr);

p.StructExpand = true;
p.parse(rawSimData,periods,varargin{:});
args = p.ResultsMassaged();

%% Extract some required constants from the rawSimData

M=length(args.rawSimData);
if M~=1
    tempSimData=args.rawSimData{1};
else
    tempSimData=args.rawSimData;
end

I=isfield(tempSimData,{'sim1_HB','sim1_TRAN'});
if I(1) %ENV
    sim='HB';
    N=length(tempSimData.(['sim1_' sim]).sweepvar_time)./args.periods;
    T=length(tempSimData.(['sim1_' sim]).freq);
elseif I(2) %TRAN
    sim='TRAN';
    N=(length(tempSimData.(['sim1_' sim]).time)-1)./args.periods;
    T=1;
else
    error('The simulation data was of an unknown type.');
end

%% Calculate the Root-Mean-Square error of all the periods using the 
% last period as the reference

tempSpec=zeros(M,floor(args.periods),N,T);
if isempty(args.fieldfilter)
    fields = fieldnames(tempSimData.(['sim1_' sim]));
else
    fields=args.fieldfilter;
end

switch sim
    case 'HB' %It's actually ENV! But is saved like this in the simData...
        for jj=1:length(fields)
            if ~strcmp(fields{jj},{'sweepvar_time','freq'})
                for pp=floor(args.periods):-1:1
                    if M~=1
                        for mm=1:M
                            tempSpec(mm,floor(args.periods)-(pp-1),:,:)=rawSimData{mm}.(['sim1_' sim]).(fields{jj})(end-(pp)*N+1:end-(pp-1)*N,:);
                        end
                    else
                        tempSpec(1,floor(args.periods)-(pp-1),:,:)=rawSimData.(['sim1_' sim]).(fields{jj})(end-(pp)*N+1:end-(pp-1)*N,:);
                    end
                end
                
                errorTransient.(fields{jj})=zeros(size(tempSpec));
                errorTransient.(fields{jj})(:,end,:,:)=[];
                for ii=1:args.periods-1
                    errorTransient.(fields{jj})(:,ii,:,:)=tempSpec(:,end,:,:)-tempSpec(:,ii,:,:);
                end
                
                rms.(fields{jj})=sqrt(mean(mean(abs(errorTransient.(fields{jj}).^2),4),3));
                
                errorTransient.(fields{jj})=reshape(permute(errorTransient.(fields{jj}),[1,3,2,4]),[M,N*(floor(args.periods)-1),T]);
            end
        end
    case 'TRAN'
        for jj=1:length(fields)
            if ~strcmp(fields{jj},'time')
                for pp=floor(args.periods):-1:1
                    if M~=1
                        for mm=1:M
                            tempSpec(mm,floor(args.periods)-(pp-1),:)=rawSimData{mm}.(['sim1_' sim]).(fields{jj})(end-(pp)*N+1:end-(pp-1)*N);
                        end
                    else
                        tempSpec(1,floor(args.periods)-(pp-1),:)=rawSimData.(['sim1_' sim]).(fields{jj})(end-(pp)*N+1:end-(pp-1)*N);
                    end
                end
                
                errorTransient.(fields{jj})=zeros(size(tempSpec));
                errorTransient.(fields{jj})(:,end,:)=[];
                for ii=1:args.periods-1
                    errorTransient.(fields{jj})(:,ii,:)=tempSpec(:,end,:)-tempSpec(:,ii,:);
                end
                
                rms.(fields{jj})=sqrt(mean(mean(errorTransient.(fields{jj}).^2,4),3));
                
            end
        end
    otherwise
        error('impossiburu!')
end


end

% @generateFunctionHelp

% @Tagline checks transient energy in the simulation data. 

% @Description Calculates the RMS error and outputs a struct containing the error at each node.

% @Outputs{1}.Description{1} Contains the RMS-error of the periods of the rawSimData
% @Outputs{1}.Description{2} with the final period as the reference. This struct has the same
% @Outputs{1}.Description{3} structure as the rawSimdata.
% @Outputs{1}.Type struct

% @Outputs{2}.Description{1} Contains the difference (in time-domain) between the last and
% @Outputs{2}.Description{2} previous periods of the rawSimData.
% @Outputs{2}.Description{3} This struct has the same structure as the rawSimdata.
% @Outputs{2}.Type struct