function [signal,time] = ADSconvert2timeDomain(spec,varargin)
% ADSCONVERT2TIMEDOMAIN converts spectral data from the HB simulator into the time domain. 
%
%    [signal,time] =  ADSCONVERT2TIMEDOMAIN(spec)
%    [signal,time] =  ADSCONVERT2TIMEDOMAIN(spec,'ParamName',ParamValue)
%
% 
%
% Required Inputs:
%   spec  Default:  CheckFunction: @checkSpec
%     spec structure. (Output of the HB simulation)
% Parameter-Value pairs:
%   oversample  Default: 16 CheckFunction: @(x) isnumeric(x) && isscalar(x)
%     oversample ratio used when constructing the time-domain signals.
%   freq  Default: [] CheckFunction: @(x) isnumeric(x) && isvector(x)
%     frequency axis of the signal which is only used when spec is a matrix.
%     If spec is a struct, spec should have a freq field
%   fieldfilter  Default: [] CheckFunction: @iscellstr
%     cell of the fieldnames of the spec struct that should be converted,
%     when this cell array is empty all fields of spec will be converted to the time domain.
%   f0  Default: [] CheckFunction: @isscalar
%     Frequency resolution of the multisine. Necessary to give this value if
%     there are severe rounding errors in spec.freq.
% 
% Outputs: 
%   signal Type: struct
%     Contains the requested time-domain signals.
%   time Type: cell array of structs
%     Contains the time vector with the same length as the signal.
%
% 
% See Also: ADSSIMULATEHB

global Fields

p = inputParser();
% spec structure. (Output of the HB simulation)
p.addRequired('spec',@checkSpec);
% oversample ratio used when constructing the time-domain signals.
p.addParamValue('oversample',16,@(x) isnumeric(x) && isscalar(x));
% frequency axis of the signal which is only used when spec is a matrix.
% If spec is a struct, spec should have a freq field
p.addParamValue('freq',[],@(x) isnumeric(x) && isvector(x));
% cell of the fieldnames of the spec struct that should be converted, 
% when this cell array is empty all fields of spec will be converted to the time domain.
p.addParamValue('fieldfilter',[],@iscellstr);
% Frequency resolution of the multisine. Necessary to give this value if
% there are severe rounding errors in spec.freq.
p.addParamValue('f0',[],@isscalar);
p.StructExpand = true;
p.parse(spec,varargin{:});
args = p.Results();

% do some more checks on the spec
if isnumeric(args.spec)
    specIsMatrix = true;
    if isempty(args.freq)
        error('if you provide spec as a matrix, you also need to pass the frequency axis')
    else
        F = length(args.freq);
        siz = size(args.spec);
        if length(siz)>3
            error('spec can have maximum 3 dimensions');
        end
        if siz(end)~=F
            error('the final dimension of spec should be equal to length(spec)')
        end
        % put the matrix in the struct and continue like spec is a struct
        args.spec.signal = args.spec;
        args.spec.freq = args.freq;
        Fields = {'signal'};
    end
else
    specIsMatrix = false;
end

%Check if the field of fieldfilter is available
if ~isempty(args.fieldfilter)
    for ff=1:length(args.fieldfilter)
        if ~any(strcmp(args.fieldfilter{ff}, Fields))
            error('the field provided in the fieldfilter is not a good field')
        end
    end
    Fields = args.fieldfilter;
end

if isempty(args.f0)
    f0 = (mode(abs(args.spec.freq-circshift(args.spec.freq,[0,1]))));
else
    f0=args.f0;
end

%Set the sample frequency, time vector length and the time vector
fs=(f0*args.spec.freq(end)/f0+f0)*2*args.oversample;
N=round(fs/f0);
time=0:1/fs:1/f0-1/fs;

%Check the dimensions of the spectral data
DIMS = length(size(args.spec.(Fields{1})));
switch DIMS
    case 3
        [M,P,~]=size(args.spec.(Fields{1}));
        dummy=zeros(M,P,N);
    case 2
        [M,~]=size(args.spec.(Fields{1}));
        dummy=zeros(M,N);
    otherwise
        error('only data up to three dimensions is supported');
end

% calculate the time domain vector
for ii=1:length(Fields)
    signal.(Fields{ii})=dummy;
    switch DIMS
        case 3
            signal.(Fields{ii})(:,:,round(args.spec.freq/f0)+1)=args.spec.(Fields{ii});
            signal.(Fields{ii})=real((ifft(signal.(Fields{ii}),[],3))*N);
        case 2
            signal.(Fields{ii})(:,round(args.spec.freq/f0)+1)=args.spec.(Fields{ii});
            signal.(Fields{ii})=real((ifft(signal.(Fields{ii}),[],2))*N);
    end
end

% if you passed a matrix to start with, the result should be a matrix as well, put it in there
if specIsMatrix
    signal = signal.signal;
end

end

%% function to check the spectrum passed to the function
function res = checkSpec(s)

global Fields

if isstruct(s)
    % if s is a struct, it should contain a freq field and at least one
    % matrix field that has its final dimension equal to length(freq)
    if isfield(s,'freq')
        F = length(s.freq);
        s = rmfield(s,'freq');
        Fields = fieldnames(s);
        % remove non-numeric fields
        toremove=[];
        for ff=1:length(Fields)
            if ~isnumeric(s.(Fields{ff}))
                toremove = [toremove ff];
            end
        end
        if ~isempty(toremove)
            warning('non-numeric fields in spec detected. They will be ignored');
            Fields = Fields(1:length(Fields)~=toremove);
        end
        % check the size of the remaining fields in spec
        siz = zeros(length(Fields),3);
        for ff=1:length(Fields)
            if length(size(s.(Fields{ff})))==2
                siz(ff,1)=1;
                siz(ff,2:3)=size(s.(Fields{ff}));
            else
                siz(ff,:)=size(s.(Fields{ff}));
            end
        end
        % the third element in the size vector should be F for all the fields
        temp = siz(:,3)~=F;
        if any(temp)
            warning('size(field,3)~=length(freq) for some of the fields. They will be ignored');
            Fields = Fields(~temp);
            siz = siz(~temp,:);
        end
        % look for weird sizes in the siz matrix.
        siz_t =siz(:,1)*max(max(siz(:,1:2)))+siz(:,2);
        M = mode(siz_t);
        if any(siz_t~=M)
            warning('some fields with a different size are detected, They will be ignored');
            Fields = Fields(siz_t==M);
            disp(Fields)
        end
    else
        error('spec.freq should be provided');
    end
else
    if ~isnumeric(s)
        % the checks for numeric spec will be performed outside of the
        % inputparser, because we need access to the freq parameter
        error('spec should be a struct or a matrix');
    end
end
% if you pass all the checks, you are happy!
res = true;
end

% @generateFunctionHelp

% @Tagline converts spectral data from the HB simulator into the time domain. 

% @Outputs{1}.Description Contains the requested time-domain signals.
% @Outputs{1}.Type struct

% @Outputs{2}.Description Contains the time vector with the same length as the signal.
% @Outputs{2}.Type cell array of structs

% @SeeAlso ADSsimulateHB