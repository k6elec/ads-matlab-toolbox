# ADS-Matlab toolbox

Collection of Matlab functions that enables to run advanced simulations in Keysight's Advanced Design System from Matlab scripts.

Important functions of this repository include:

**ADSsimulateMS**  to obtain the response of a circuit to a multisine excitation

**ADSsimulateMSwaves** to measure the waves in a circuit under multisine excitation

**ADSsimulateAC** to perform an AC simulation on a circuit

**ADSsimulateLSSS** to perform a Large-signal small-signal simulation on a circuit

**ADSconvert2timeDomain** to convert a spectrum obtained with a HB simulation to the time domain

**ADScheckStability** to check the stability of a circuit

## Example 1: Simulate a Multisine

We have the following simple circuit in a netlist file called `netlist.net`  and we wish to determine the response of this circuit to a multisine excitation.

```
R:R1 in out R=5
C:C1 out 0 C=0.1 pF
#uselib "ckt" , "NonlinVCCS"
NonlinVCCS:nlres out 0 out 0 Coeff=list(0,0.2,0.02,0.01,0.01)
```

We generate a multisine with 41 tones in a 1MHz bandwidth around 1GHz. The multisine will be connected to the `in`  node

```matlab
MSdef = MScreate(1e6,1e9,'mode','bandpass','numtones',41,'Rout',50,'amplitude',0.1);
MSdef.MSnode = 'in';
```

We run harmonic balance simulations with `ADSsimulateMS`  to obtain the response of the circuit to 7 random-phase realisations of the multisine:

```matlab
simresult=ADSsimulateMS('netlist.net',MSdef,'Simulator','HB',...
		'numberOfRealisations',7);
```

And we plot the simulation result

```matlab
figure
plot(simresult.freq,squeeze(db(simresult.out(:,1,:))).','+')
xlabel('Frequency [Hz]')
ylabel('Amplitude [dBV]')
title('Steady-state voltage spectrum')
```

**TODO:** add a picture of the resulting plot

## Example 2: Perform an LSSS simulation

We have the following simple ADS netlist which contains a mixer block.

```
Mixer:MIX1  vi vo lo SideBand=0 ConvGain=dbpolar(15,0) S11=polar(0,0) S22=polar(0,180) S33=0 GainCompType=0 ReferToInput=0 GainCompSat=5.0 dB GainComp=1.0 dB 
R:R1  vo 0 R=50 Ohm Noise=yes 
```

We add the LO source as a multisine with only one tone and 50Ohm output resistance. The LO source is  connected to the `lo`  node of the mixer: 

```matlab
MSdef = MScreate(1e9,1e9,'Rout',50);
MSdef.MSnode = 'lo';
```

Now re run the LSSS simulation. The small-signal is connected to the `vi`  node and is given an output impedance of 50Ohm. We perform a logarithmic sweep of the small-signal frequency from 1Hz to 2.4GHz with 10 points per decade.

```matlab
[res,spec] = ADSsimulateLSSS('mixer.net',MSdef,'LSSSNode','vi','Rsource',50,...
    'Start',1,'Stop',2.4e9,'Dec',10);
```

After this simulation, we extract the Harmonic Transfer functions from the simulation results

```matlab
HTFs = LSSSextractHTFs(res);
```

We plot the resulting Harmonic Transfer functions

```matlab
figure(2)
plot(res.ssfreq,db(squeeze(HTFs.vo(1,11+(-2:2),:))));
title('Harmonic Transfer Functions')
legend('HTF_{-2}','HTF_{-1}','HTF_{0}','HTF_{+1}','HTF_{+2}')
xlabel('Frequency [Hz]');
ylabel('Magnitude [dB]');
```

**TODO:** add a picture that shows the matlab plot

## Structure of the repository

The ads-matlab toolbox is a collection of several repositories organised as git submodules. 

![](gitstructure.svg)