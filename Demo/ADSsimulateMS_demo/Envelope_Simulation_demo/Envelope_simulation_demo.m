% in this demo, I try the different integration methods for envelope
% simulation on the same circuit. To see their influence on the result

% The test circuit consists of a tanh static non-linearity 
% followed by an elliptic filter with center frequency of 10GHz. and a bandwidth of 200MHz
% its transmission zeroes are placed at 9.960GHz and 10.32GHz.
clear variables
close all
clc

% I give the multisine a small DC offset to obtain even-order non-linearities as well
MSdef = MScreate(1e6,10e9,'mode','bandpass','bandwidth',200e6,'numTones',41,'DC',1,'rms',0.2);
MSdef.MSnode = 'in';

% generate a phase vector for the multisine
phase = zeros(length(MSdef.phase),1,1);
phase(:,1,1) = 2*pi*rand(size(MSdef.phase));

% simulate with HB to get the correct answer
HB = ADSsimulateMS('Envelope_simulation_demo.net',MSdef,'simulator','HB','numberOfRealisations',1,'phase',phase);

% simulate the system using trapezoidal integration
ENV_TRAP = ADSsimulateMS('Envelope_simulation_demo.net',MSdef,'simulator','ENV','numberOfRealisations',1,'phase',phase,...
    'EnvIntegOrder',2,'EnvUseGear',0,'periods',10);

% simulate the system using gear's order two integration
ENV_GEAR = ADSsimulateMS('Envelope_simulation_demo.net',MSdef,'simulator','ENV','numberOfRealisations',1,'phase',phase,...
    'EnvIntegOrder',2,'EnvUseGear',1,'periods',10);

% simulate the system using backward euler integration
ENV_BEUL = ADSsimulateMS('Envelope_simulation_demo.net',MSdef,'simulator','ENV','numberOfRealisations',1,'phase',phase,...
    'EnvIntegOrder',1,'EnvUseGear',0,'periods',10);

% find all the bins around 10GHz
fundBins_HB = find((HB.freq>5e9)&(HB.freq<15e9));
fundBins_ENV = find((ENV_TRAP.freq>5e9)&(ENV_TRAP.freq<15e9));

% calculate the filter response
FILTER_HB = squeeze(HB.out(:,end,fundBins_HB))./squeeze(HB.nl(:,end,fundBins_HB));
FILTER_ENV_TRAP = squeeze(ENV_TRAP.out(:,end,fundBins_ENV))./squeeze(ENV_TRAP.nl(:,end,fundBins_ENV));
FILTER_ENV_GEAR = squeeze(ENV_GEAR.out(:,end,fundBins_ENV))./squeeze(ENV_GEAR.nl(:,end,fundBins_ENV));
FILTER_ENV_BEUL = squeeze(ENV_BEUL.out(:,end,fundBins_ENV))./squeeze(ENV_BEUL.nl(:,end,fundBins_ENV));

% plot the spectra at the output and after the non-linear block
figure(1)
clf
subplot(121)
hold all
plot(HB.freq(fundBins_HB)/1e9,db(squeeze(HB.nl(:,end,fundBins_HB))),'+');
plot(ENV_TRAP.freq(fundBins_ENV)/1e9,db(squeeze(ENV_TRAP.nl(:,end,fundBins_ENV))),'o');
plot(ENV_GEAR.freq(fundBins_ENV)/1e9,db(squeeze(ENV_GEAR.nl(:,end,fundBins_ENV))),'x');
plot(ENV_BEUL.freq(fundBins_ENV)/1e9,db(squeeze(ENV_BEUL.nl(:,end,fundBins_ENV))),'.');
xlim([ENV_TRAP.freq(fundBins_ENV(1)) ENV_TRAP.freq(fundBins_ENV(end))]/1e9);
xlabel('Frequency [GHz]')
ylabel('Voltage [dBV]')
legend('HB','TRAP','GEAR','BEUL','Location','S')
title('NL')
subplot(122)
hold all
plot(HB.freq(fundBins_HB)/1e9,db(squeeze(HB.out(:,end,fundBins_HB))),'+');
plot(ENV_TRAP.freq(fundBins_ENV)/1e9,db(squeeze(ENV_TRAP.out(:,end,fundBins_ENV))),'o');
plot(ENV_GEAR.freq(fundBins_ENV)/1e9,db(squeeze(ENV_GEAR.out(:,end,fundBins_ENV))),'x');
plot(ENV_BEUL.freq(fundBins_ENV)/1e9,db(squeeze(ENV_BEUL.out(:,end,fundBins_ENV))),'.');
xlim([ENV_TRAP.freq(fundBins_ENV(1)) ENV_TRAP.freq(fundBins_ENV(end))]/1e9);
xlabel('Frequency [GHz]')
ylabel('Voltage [dBV]')
legend('HB','TRAP','GEAR','BEUL','Location','S')
title('OUT')

% plot the obtained filter FRF in magnitude and phase
figure(2)
clf
subplot(121)
hold all
plot(HB.freq(fundBins_HB)/1e9,db(FILTER_HB),'Linewidth',2);
plot(ENV_TRAP.freq(fundBins_ENV)/1e9,db(FILTER_ENV_TRAP));
plot(ENV_GEAR.freq(fundBins_ENV)/1e9,db(FILTER_ENV_GEAR));
plot(ENV_BEUL.freq(fundBins_ENV)/1e9,db(FILTER_ENV_BEUL));
legend('HB','TRAP','GEAR','BEUL')
ylim([-100 0]);
xlim([ENV_TRAP.freq(fundBins_ENV(1)) ENV_TRAP.freq(fundBins_ENV(end))]/1e9);
xlabel('Frequency [GHz]')
ylabel('Magnitude [dB]')
subplot(122)
hold all
plot(HB.freq(fundBins_HB)/1e9,angle(FILTER_HB),'Linewidth',2);
plot(ENV_TRAP.freq(fundBins_ENV)/1e9,angle(FILTER_ENV_TRAP));
plot(ENV_GEAR.freq(fundBins_ENV)/1e9,angle(FILTER_ENV_GEAR));
plot(ENV_BEUL.freq(fundBins_ENV)/1e9,angle(FILTER_ENV_BEUL));
legend('HB','TRAP','GEAR','BEUL')
ylim([-pi pi]);
xlim([ENV_TRAP.freq(fundBins_ENV(1)) ENV_TRAP.freq(fundBins_ENV(end))]/1e9);
xlabel('Frequency [GHz]')
ylabel('Phase [rad]')

