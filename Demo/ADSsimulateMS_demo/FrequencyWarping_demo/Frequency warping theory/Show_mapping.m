% This scrips shows the influence of the different integration methods in a
% time-domain simulation on the location of the poles. This is also
% discussed in
% K. Kundert "The designer's guide to spice and spectre" kluwer academic, 1995
clear all
close all
clc

% T is the sample time
T = 5;

% IntegMethod = 'backward euler';
IntegMethod = 'backward euler';
switch IntegMethod
    case 'Trapezoidal'
        transform = @(z) (2+z*T)./(2-z*T);
        inverseTransform = @(z) (2/T)*(z-1)./(1+z);
    case 'backward euler'
        transform = @(z) 1./(1-z*T);
        inverseTransform = @(z) 1-1./z;
    case 'gear2'
        transform = @(z) [(-2-sqrt(1+2*z*T))./(2*z*T-3) (-2+sqrt(1+2*z*T))./(2*z*T-3)];
        inverseTransform = @(z) (3*z.*z-4*z+1)./(2*T*z.*z);
    case 'forward euler'
        transform = @(z) 1+z*T;
        inverseTransform = @(z) (z-1)/T;
    otherwise
        error('Integration method is not known');
end

% generate a mesh grid
realstepsneg = -10 : 0.5 : 0;
realstepspos = 0 : 0.5 : 5;
imagsteps = -5 : 0.2 : 5;

% this controls the steps of the transform
res = 0.01;

figure(1)
clf
% draw all vertical real lines in the left half-plane
for rr=1:length(realstepsneg)
    number = realstepsneg(rr)+1i*(min(imagsteps):res:max(imagsteps));
    trans = transform(number);
    subplot(121)
    plot(real(number),imag(number),'color',[0 0 1]);
    if rr==1; hold on; end
    subplot(122)
    plot(real(trans),imag(trans),'color',[0 0 1]);
    if rr==1; hold on; end
end
% plot the vertical lines in the right half-plane
for rr=1:length(realstepspos)
    number = realstepspos(rr)+1i*(min(imagsteps):res:max(imagsteps));
    trans = transform(number);
    subplot(121)
    plot(real(number),imag(number),'color',[1 realstepspos(rr)/max(realstepspos) realstepspos(rr)/max(realstepspos)]);
    subplot(122)
    plot(real(trans),imag(trans),'color',[1 realstepspos(rr)/max(realstepspos) realstepspos(rr)/max(realstepspos)]);
end
% draw the imaginary axis
number = 1i*(min(imagsteps):res:max(imagsteps));
trans = transform(number);
plot(real(trans),imag(trans),'g','linewidth',2);
% draw the horizontal lines on the s-plane
for rr=1:length(imagsteps)
    number = 1i*imagsteps(rr)+(min(realstepsneg):res:max(realstepsneg));
    trans = transform(number);
    subplot(121)
    plot(real(number),imag(number),'-','color',[0 0 1]);
    subplot(122)
    plot(real(trans),imag(trans),'-','color',[0 0 1]);
end
for rr=1:length(imagsteps)
    number = 1i*imagsteps(rr)+(min(realstepspos):res:max(realstepspos));
    trans = transform(number);
    subplot(121)
    plot(real(number),imag(number),'color',[1 0 0]);
    subplot(122)
    plot(real(trans),imag(trans),'color',[1 0 0]);
end

% also draw the unit circle
subplot(122)
plot(exp(1i*2*pi*linspace(0,1,1000)),'k','Linewidth',1);
xlim([-1 1])
ylim([-1 1])

% plot the real and imaginary axis
subplot(121)
plot([0 0],[min(imagsteps) max(imagsteps)],'g','Linewidth',2);
plot([min(realstepsneg) max(realstepspos)],[0 0],'k','Linewidth',2);


%% inverse transform

radiiin = 0.1:0.1:1;
radiiout= 1:0.1:2;

angles = linspace(0,2*pi,20);angles = angles(2:end-1);

figure(2)
clf
for rr=1:length(radiiin)
    number = radiiin(rr).*exp(1i*linspace(0,2*pi,500));
    subplot(121)
    plot(real(number),imag(number),'color',[radiiin(rr) 0 1])
    hold on
    subplot(122)
    plot(inverseTransform(number),'color',[radiiin(rr) 0 1]);
    hold on
end
for rr=1:length(radiiout)
    number = radiiout(rr).*exp(1i*linspace(0,2*pi,500));
    subplot(121)
    plot(real(number),imag(number),'color',[1 0 0])
    hold on
    subplot(122)
    plot(inverseTransform(number),'color',[1 0 0]);
    hold on
end

for rr=1:length(angles)
    number = linspace(0,1,50).*exp(1i*angles(rr));
    subplot(121)
    plot(real(number),imag(number),'color',[0 0 1]);
    hold on
    subplot(122)
    plot(inverseTransform(number),'color',[0 0 1]);
    hold on
end

for rr=1:length(angles)
    number = linspace(1,max(radiiout),50).*exp(1i*angles(rr));
    subplot(121)
    plot(real(number),imag(number),'color',[1 0 0]);
    hold on
    subplot(122)
    plot(inverseTransform(number),'color',[1 0 0]);
    hold on
end
% the map of the unit circle deserves a separate plot


number = exp(1i*linspace(0,2*pi,500));
subplot(121)
plot(number,'g','linewidth',2)
subplot(122)
plot(inverseTransform(number),'g','linewidth',2);

% draw the real and imaginary axis
plot([0 0],[-10 10],'k');
plot([-10 10],[0 0],'k');


% plot(inverseTransform(exp(1i*linspace(0,2*pi,500))),'g','Linewidth','g');
% plot the inverse transform of the map of the imaginary axis. This should
% translate onto the imaginary axis
% plot(inverseTransform(0.5*exp(1i*linspace(0,2*pi,1000))+0.5),'r');
xlim([-10 10]);
ylim([-10 10]);






