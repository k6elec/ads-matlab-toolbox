% in this script, I show the effect of frequency warping in transient and
% envelope simulations

% The netlist contains a fourth order elliptic filter centered around 10GHz 
% with a bandwidth of 200 MHz. Its transmission zeroes are located at
% 9.960GHz and 10.32GHz.
clear all
close all
clc

% build a multisine around 10GHz with a bandwidth of 1GHz. 
% The amount of tones has to be an odd number
MSdef = MScreate(1e6,10e9,'mode','bandpass','bandwidth',1e9,'numTones',101,'rms',1);
MSdef.MSnode = 'in';


% P is the amount of periods that are simulated
P = 20;

% determine the response with a transient simulation.
% I use an oversample of 10, An oversample of 2 introduces so much warping
% that the filter is completely missed. An oversample of 10 gives some
% frequency warping, but not too much...
[TRAN,rawTRAN] = ADSsimulateMS('Freqwarping_demo.net',MSdef,'simulator','TRAN','numberOfRealisations',1,'periods',P,'oversample',10,'freqWarping',false);

% determine the response with an envelope simulation.
% I use an EnvOversample of 2 to show the warping nicely
% Alongside trapezoidal I also simulate the system with Backward Euler and
% Gear2 To get an idea of the difference in warping/damping.
% BACKWARD EULER
[ENV_BE,rawENV_BE] = ADSsimulateMS('Freqwarping_demo.net',MSdef,'simulator','ENV','numberOfRealisations',1,'EnvOversample',2,'periods',P,'EnvIntegOrder',1);
% TRAPEZOIDAL
[ENV_TRAP,rawENV_TRAP] = ADSsimulateMS('Freqwarping_demo.net',MSdef,'simulator','ENV','numberOfRealisations',1,'EnvOversample',2,'periods',P,'EnvIntegOrder',2,...
    'freqWarping',false);
% GEAR2
[ENV_G2,rawENV_G2] = ADSsimulateMS('Freqwarping_demo.net',MSdef,'simulator','ENV','numberOfRealisations',1,'EnvOversample',2,'periods',P,'EnvIntegOrder',2, ...
    'EnvUseGear',1);

% determine the response with an HB simulation. 
% This response has no warping in it and is the de facto reference
% spectrum.
HB = ADSsimulateMS('Freqwarping_demo.net',MSdef,'simulator','HB','numberOfRealisations',1,'oversample',2);

% find the excited bins in the different simulations
exBinsTRAN = squeeze(db(TRAN.in(end,end,:))>-100);
exBinsHB   = squeeze(db(  HB.in(end,end,:))>-100);
exBinsENV  = squeeze(db( ENV_BE.in(end,end,:))>-100);

%% Calculate the transfer functions of the filter
% Necessary if we want to compare the phase of the different simulation
% results.

TRAN_FRF=squeeze(TRAN.out(1,end,exBinsTRAN))./squeeze(TRAN.in(1,end,exBinsTRAN));
HB_FRF=squeeze(HB.out(1,end,exBinsHB))./squeeze(HB.in(1,end,exBinsHB));
ENV_BE_FRF=squeeze(ENV_BE.out(1,end,exBinsENV))./squeeze(ENV_BE.in(1,end,exBinsENV));
ENV_TRAP_FRF=squeeze(ENV_TRAP.out(1,end,exBinsENV))./squeeze(ENV_TRAP.in(1,end,exBinsENV));
ENV_G2_FRF=squeeze(ENV_G2.out(1,end,exBinsENV))./squeeze(ENV_G2.in(1,end,exBinsENV));

% plot the results
figure(1)
title('Filter characteristic using different integration methods');
clf
subplot(211);
hold on
plot(TRAN.freq(exBinsTRAN)/1e9,db(TRAN_FRF),'b');
plot(  HB.freq(exBinsHB  )/1e9,db(HB_FRF),'r');
plot( ENV_BE.freq(exBinsENV )/1e9,db(ENV_BE_FRF),'m');
plot( ENV_TRAP.freq(exBinsENV )/1e9,db(ENV_TRAP_FRF),'Color',[0,0.5,0]);
plot( ENV_G2.freq(exBinsENV )/1e9,db(ENV_G2_FRF),'k');
legend('TRAN','HB','ENV_{BEUL}','ENV_{TRAP}','ENV_{GEAR2}');
grid on

subplot(212);
hold on
plot(TRAN.freq(exBinsTRAN)/1e9,unwrap(angle(TRAN_FRF)),'b');
plot(  HB.freq(exBinsHB  )/1e9,unwrap(angle(HB_FRF)),'r');
plot( ENV_BE.freq(exBinsENV )/1e9,unwrap(angle(ENV_BE_FRF)),'m');
plot( ENV_TRAP.freq(exBinsENV )/1e9,unwrap(angle(ENV_TRAP_FRF)),'Color',[0,0.5,0]);
plot( ENV_G2.freq(exBinsENV)/1e9,unwrap(angle(ENV_G2_FRF)),'k');
legend('TRAN','HB','ENV_{BEUL}','ENV_{TRAP}','ENV_{GEAR2}');
grid on

%% Try to remove the frequency warping from the Trapezoidal Envelope Simulation
timeStep=5.0000e-10;
fs = 1/timeStep;

modFreq=ENV_TRAP.freq(exBinsENV);
modFreq=modFreq-10e9;
modFreq=(fs/pi)*tan(pi*modFreq/fs);
modFreq=modFreq+10e9;

newFreq=ENV_TRAP.freq;
newFreq(exBinsENV)=modFreq;

% plot the results
figure(2)
title('Comparison of (un)compensated filter characteristics');
clf
hold on
plot(HB.freq(exBinsHB  )/1e9,db(HB_FRF),'r');
plot(ENV_TRAP.freq(exBinsENV)/1e9,db(ENV_TRAP_FRF),'m');
plot(newFreq(exBinsENV )/1e9,db(ENV_TRAP_FRF),'kx-');
legend('HB (reference)','ENV_{TRAP}','ENV_{TRAP} (compensated)');
grid on

%% Calculate the transient error that is still present in each of the simulations

[rmsTRAN    ,TRAN_errorTransient    ] = checkTransients(rawTRAN    ,P);
[rmsENV_BE  ,ENV_BE_errorTransient  ] = checkTransients(rawENV_BE  ,P);
[rmsENV_TRAP,ENV_TRAP_errorTransient] = checkTransients(rawENV_TRAP,P);
[rmsENV_G2  ,ENV_G2_errorTransient  ] = checkTransients(rawENV_G2  ,P);

figure(3)
title('RMS of the lingering transient misery');
hold on
plot(db(rmsTRAN.out(1,:)),'b');
plot(db(rmsENV_BE.out(1,:)),'m');
plot(db(rmsENV_TRAP.out(1,:)),'r');
plot(db(rmsENV_G2.out(1,:)),'k');
grid on
legend('TRAN','ENV_{BEUL}','ENV_{TRAP}','ENV_{GEAR2}')

figure(4)
title('Transient behaviour of TRAN');
hold on
plot(TRAN_errorTransient.out(1,:),'b');
grid on
legend('TRAN')

figure(5)
title('Transient behaviour of ENV');
hold on
plot(db(ENV_BE_errorTransient.out(1,:,2)),'r');
plot(db(ENV_TRAP_errorTransient.out(1,:,2)),'b');
plot(db(ENV_G2_errorTransient.out(1,:,2)),'m');
legend('ENV_{BEUL}','ENV_{TRAP}','ENV_{GEAR2}')
grid on


