% This demo shows how to simulate the response of a circuit to a multisine
% using ADS

clear variables
close all
clc

cd(fileparts(mfilename('fullpath')));

%% Configure the Matlab right

% set the preferences of the matlab right. This has to be done only once on
% each computer. You'll have to edit this file to set the configuration
% right for your computer and Z-drive
% set_preferences_ADS2014


%% Build the Multisine

% use the MScreate function to generate a bandpass multisine around 1GHz
MSdef = MScreate(1e6,1e9,'mode','bandpass','numtones',41,'Rout',50,'amplitude',0.1);

% the MSnode field indicates the node the multisine source is connected to.
% if MSnode is a cell array of two strings, the source is connected in between the two provided nodes
MSdef.MSnode = 'in';

M=6;
phase = 2*pi*(rand([length(MSdef.grid) M 1])-0.5);

%% Run the simulation
% The multisine is a bandpass multisine around 1GHZ, we best use harmonic
% balance to simulate such a structure.
% The function will automatically run 7 phase realisations of the multisine
% on the circuit.

setpref('ADS','parallelSims',3);


simresult=ADSsimulateMS('ADSsimulateMS_demo_basic_Netlist.net',MSdef,'Simulator','HB','numberOfRealisations',M,'phase',phase,'CleanupFiles',true);


%% Look at the results
% We look at the voltage spectrum at the output node.
figure
plot(simresult.freq,squeeze(db(simresult.out(:,1,:))).','+')
xlabel('Frequency [Hz]')
ylabel('Amplitude [dBV]')
title('Steady-state voltage spectrum')

% and also look at the time domain signal
figure
[signal,time] = ADSconvert2timeDomain(simresult);
plot(time,squeeze(signal.out(:,1,:)).')
xlabel('Time [s]');
ylabel('Voltage [V]');
title('Time domain waveform of the steady-state')
