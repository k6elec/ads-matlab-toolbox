% This script compares the three different simulators in ADSsimulateMS on
% the same circuit
cd('V:\Toolbox\ADStoolbox\Demo\ADSsimulateMS_demo\Basic SimulateMS demo\')
clear all
close all
clc

% use the MScreate function to generate a bandpass multisine around 1GHz
MSdef = MScreate(1e6,1e9,'mode','bandpass','numtones',41,'Rout',50,'amplitude',0.1);

% the multisine is connected to the 'in' node
MSdef.MSnode = 'in';

% We will simulate only one realisation with a fixed phase for the multisine
phase = 2*pi*(rand([length(MSdef.grid) 1 1])-0.5);

% Now run the simulations
% Harmonic Balance first
tic
hb  = ADSsimulateMS('ADSsimulateMS_demo_basic_Netlist.net',MSdef,'Simulator','HB','phase',phase);
hbtoc = toc;

%%
% now the envelope simulation
tic
env = ADSsimulateMS('ADSsimulateMS_demo_basic_Netlist.net',MSdef,'Simulator','ENV','phase',phase);
envtoc = toc;
% and finally, the Transient simulation 
tic
tran = ADSsimulateMS('ADSsimulateMS_demo_basic_Netlist.net',MSdef,'Simulator','TRAN','phase',phase);
trantoc = toc;

clc
% show the simulation time, to compare. On this problem, the simulation
% time is about the same for the three different simulators
fprintf(1,'  HB time: %ss\n',eng(hbtoc  ));
fprintf(1,' ENV time: %ss\n',eng(envtoc ));
fprintf(1,'TRAN time: %ss\n',eng(trantoc));

% Look at the results
% We compare the voltage spectrum at the output node.
close all
figure
hold on
plot(   hb.freq , squeeze(db(    hb.out(:,end,:))).' , 'r+' )
plot(  env.freq , squeeze(db(   env.out(:,end,:))).' , 'bo' )
plot( tran.freq , squeeze(db(  tran.out(:,end,:))).' , 'k.' )
legend('HB','ENV','TRAN');
xlabel('frequency [Hz]');
ylabel('amplitude [db]');

