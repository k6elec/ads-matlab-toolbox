% To run this demo, you must make sure that the FrequencyDomain toolbox is
% on your matlab path.
clear variables
close all
clc
cd(fileparts(mfilename('fullpath')));

%% Generate the Multisine
% I use the ExcitationDesign toolbox, which allows me to generate
% multisines and write them to files that can be used by the simulator.

% Generate the multisine used in the method
MSdef = MScreate(1e4,1e6,'grid','random-odd','rms',0.001,'Rout',0);

% MSnode is the node where the multisine is connected to, in this case, the
% multisine has to be connected between the 'in' node and the 'sgnd' node
MSdef.MSnode = {'in','sgnd'};

% simulate the whole thing in ADS
simresult = ADSsimulateMS('opamp.net',MSdef,'simulator','HB','numberOfRealisations',9);
% running the simulations requires access to the UMC180 design kit on the vagevuur drive

%% show the results

figure(1)
plot(simresult.freq , squeeze(db(simresult.in(:,1,:))),'+')
title('reference signal')
xlabel('Frequency [Hz]');
ylabel('Amplitude [dBV]');

figure(2)
plot(simresult.freq , squeeze(db(simresult.amp(:,1,:))),'+')
title('input signal')
xlabel('Frequency [Hz]');
ylabel('Amplitude [dBV]');

figure(3)
plot(simresult.freq , squeeze(db(simresult.out(:,1,:))),'+')
title('output signal')
xlabel('Frequency [Hz]');
ylabel('Amplitude [dBV]');

%% calculate the BLA
% calculateSISO_BLA does the work for us. It calculates the BLA using Rik's
% Robust_NL_Anal and performs the spectral correction needed to determine
% the distortion in the circuit

[G, Y, U, exBins , CovYs ] = calculateSISO_BLA(simresult,'input','amp','output','out','reference','in');

figure
semilogx(G.freq,db(G.mean),'k+-');
hold on
semilogx(G.freq,db(G.stdNL),'b');
legend('BLA','\sigma_{BLA}')
grid on
xlabel('Frequency [Hz]');
ylabel('Magnitude [dB]');



