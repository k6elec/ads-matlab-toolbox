function theoHTF = HTFs_firstOrder_theoretic(f,alpha,fsys,numHTFs,I,output)
% 
%
%   theoHTF = HTFs_firstOrder_theoretic(f,alpha,fsys,numHTFs)
%   theoHTF = HTFs_firstOrder_theoretic(f,alpha,fsys,numHTFs,internalOrder)
%
%

if ~exist('internalOrder','var')
    I = numHTFs*10;
end

if ~exist('output','var')
    output = 'HTF';
end

f=f(:);

switch output
    case 'HTF'
        % check the result of the calculation by implementing the bessel stuff of Ebrahim
        theoHTF = zeros(2*numHTFs+1,length(f));
        for htf=-numHTFs:numHTFs
            for n=-I:I
                theoHTF(htf+numHTFs+1,:) = theoHTF(htf+numHTFs+1,:) + (besselj(htf-n,1i*alpha/(2*pi*fsys))*besselj(-n,1i*alpha/(2*pi*fsys))./(1i*(2*pi*f+n*2*pi*fsys)+1)).';
            end
        end
    case 'ss'
        
        n=-I:I;
        % p contains the poles of the HTFs
        % they are common-denominator, so there's only one group of poles
        % for all HTFs
        p = -1 - n*1i*2*pi*fsys;
        
        % r contains the residues of the HTFs
        r = zeros(2*numHTFs+1,2*I+1);
        for htf=-numHTFs:numHTFs
            for n=-I:I
                r(htf+numHTFs+1,n+I+1) = besselj(htf-n,1i*alpha/(2*pi*fsys))*besselj(-n,1i*alpha/(2*pi*fsys));
            end
        end
end


end