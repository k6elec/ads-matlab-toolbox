% this script shows how to use the ADSsimulateLSSS function to simulate the
% small-signal response of a mixer

% generate the large signal tone as a multisine with only one tone
MSdef = MScreate(1e9,1e9,'Rout',50);

% connect the multisine to the lo node
MSdef.MSnode = 'lo';

% call ADSsimulateLSSS with the following parameters:
%   the netlist: 'mixer.net'
%   the multisine: MSdef
%   the node for the small-signal excitation (LSSSNode): 'vi'
%   the sweep for the small-signal tone: Start, Stop and Step gives a linear sweep
%   the output impedance of the small-signal source (Rsource): 50 Ohm
[res,spec] = ADSsimulateLSSS('mixer.net',MSdef,'LSSSNode','vi',...
    'Start',1,'Stop',2.4e9,'Dec',10,'Rsource',50);

% The simulation data should be processed into harmonic transfer functions
HTFs = LSSSextractHTFs(res);

% plot the results. The ideal mixer only has HTF+1 different from zero
figure(2)
plot(res.ssfreq,db(squeeze(HTFs.vo(1,11+(-2:2),:))));
title('Harmonic Transfer Functions')
legend('HTF_{-2}','HTF_{-1}','HTF_{0}','HTF_{+1}','HTF_{+2}')
xlabel('Frequency [Hz]');
ylabel('Magnitude [dB]');
