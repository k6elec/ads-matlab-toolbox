% this script shows how to extract the harmonic transfer functions from the
% results of the small signal simulation with the LSSSextractHTFs function
% Also, the results from ADS are compared to theoretical results from the
% PhD of Ebrahim Louarroudi
clear variables
close all
clc

cd(fileparts(mfilename('fullpath')));

% generate the multisine that sets the time-variation
MSdef = MScreate(1,1);
MSdef.MSnode = {'sched'};
MSdef.ampl = -1;

% run the LSSS simulation in ADS
[res] = ADSsimulateLSSS('firstOrderSystemSimulation.net',MSdef,...
    'Start',0.001,'Stop',100,'Dec',20,'LSSSnode','in','Rsource',0,'phase',0);
% extract the Harmonic Transfer Functions
HTFs = LSSSextractHTFs(res);

% calculate the same HTFs using the theoretic result from Ebrahim's PhD
theoHTF = HTFs_firstOrder_theoretic(res.ssfreq,MSdef.ampl,MSdef.f0,10,100,'HTF');

%% plot the result
figure(1)
clf
% plot the result of the simulation
semilogx(HTFs.freq,squeeze(db(HTFs.out(1,:,:))))
hold on
% plot the theoretic HTFs
semilogx(HTFs.freq,squeeze(db(theoHTF)),'+')

% plot the difference between the HTFs
semilogx(HTFs.freq,db(theoHTF-squeeze(HTFs.out(1,:,:))),'k.')

xlabel('Frequency [Hz]');
ylabel('Magnitude [dB]');
title('- ADS result      + Theoretic    black: difference')

