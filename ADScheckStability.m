function [StabRes,Z,freq,leg,SIM,SPEC] = ADScheckStability(Netlist,MSdef,varargin)
% ADSCHECKSTABILITY checks the local stability of a solution of a circuit.
%
%    [StabRes,Z,freq,leg,SIM,SPEC] =  ADSCHECKSTABILITY(Netlist,MSdef)
%    [StabRes,Z,freq,leg,SIM,SPEC] =  ADSCHECKSTABILITY(Netlist,MSdef,'ParamName',ParamValue)
%
% 
%
% Required Inputs:
%   Netlist  Default:  CheckFunction: @(x) iscellstr(x)|ischar(x)
%     ADS netlist that describes the circuit. The main multisine source should
%     not be present in this netlist, it will be added by the function itself
%   MSdef  Default:  CheckFunction: @isstruct
%     struct describing the multisine that will set the orbit to be analysed.
%     If you want a stability analysis around DC, set the Amplitude of the
%     multisine source to zero.
% Parameter-Value pairs:
%   nodes  Default: {} CheckFunction: @iscellstr
%     list of nodes where the tickler tones are connected to. If the list is
%     empty, the function will first run a DC simulation to determine the nodes
%     present in the circuit and will then excite all available nodes
%   Start  Default: 1 CheckFunction: @isscalar
%     start frequency of the small-signal sweep. The default frequency sweep is
%     a logarithmic sweep from 1Hz to 10GHz with 100 points per decade.
%   Stop  Default: 10e11 CheckFunction: @isscalar
%     stop frequency of the small-signal sweep
%   Step  Default: [] CheckFunction: @isscalar
%     frequency step used in a linear small-signal sweep
%   Dec  Default: 400 CheckFunction: @isscalar
%     points per decade when a logarithmic sweep is used.
%   FFTpoints  Default: 2^14 CheckFunction: @isnatural
%     number of points used in the FFT during the stable/unstable projection
%   HBsettings  Default: [] CheckFunction: @isstruct
%     You can provide this to obtain custom HB settings. This option is very
%     usefull in an oscillator analysis
%   excludeNodes  Default: {} CheckFunction: @iscellstr
%     list of nodes that should not be used in the stability analysis
%   StabilityAnalysisTechnique  Default: 'PROJECTION' CheckFunction: @(x) any(strcmpi(x,{'POLEZERO','PROJECTION','SKIP'}))
%     technique used to determine the stability of the orbit
%   numHTFs  Default: 3 CheckFunction: @isnatural
%     number of HTFs to be included in the analysis
%   minOrder  Default: 1 CheckFunction: @isnatural
%     minimum order to be estimated
%   maxOrder  Default: 20 CheckFunction: @isnatural
%     maximum order to be estimated
%   orderStep  Default: 1 CheckFunction: @isnatural
%     step used in the order sweep
%   asymptotic  Default: 1 CheckFunction: @(x) any(x==[1 2 3])
%     asymptotic behaviour of the circuit at infinite frequency. This
%     determines wheter a proper, strictly proper or improper model is fitted.
%     if asymptotic=1, a strictly proper model is estimated. If asymptotic=2, a
%     proper model is estimated, if asymptotic=3, an improper model, with an
%     extra zero is estimated.
%   FourierCoeffs  Default: 300 CheckFunction: @isnatural
%     
% 
% Outputs: 
%   StabRes Type: struct
%     This struct contains the results of the stability analysis. If
%     pole-zero fitting is used, it contains the results of the vector fitting
%     routines for different orders.
%   Z Type: matrix
%     NxF matrix of impedance functions found in the linearisation of
%     the circuit. If a large-signal orbit is considered, the HTFs are also
%     added to Z
%   freq Type: 
%     frequency vector of the found impedances
%   leg Type: cell array
%     legend of the impedances
%   SIM Type: struct
%     raw simulation results from ADS
%   SPEC Type: struct
%     Spectrum of the Large-signal solution
%
% 
% See Also: 

p = inputParser();
p.FunctionName = 'ADScheckStability';
p.PartialMatching = true;
p.KeepUnmatched = true;
% ADS netlist that describes the circuit. The main multisine source should
% not be present in this netlist, it will be added by the function itself
p.addRequired('Netlist',@(x) iscellstr(x)|ischar(x));
% struct describing the multisine that will set the orbit to be analysed.
% If you want a stability analysis around DC, set the Amplitude of the
% multisine source to zero.
p.addRequired('MSdef',@isstruct);
% list of nodes where the tickler tones are connected to. If the list is
% empty, the function will first run a DC simulation to determine the nodes
% present in the circuit and will then excite all available nodes
p.addParameter('nodes',{},@iscellstr);
% start frequency of the small-signal sweep. The default frequency sweep is
% a logarithmic sweep from 1Hz to 10GHz with 100 points per decade.
p.addParameter('Start',1,@isscalar);
% stop frequency of the small-signal sweep
p.addParameter('Stop',10e11,@isscalar);
% frequency step used in a linear small-signal sweep
p.addParameter('Step',[],@isscalar);
% points per decade when a logarithmic sweep is used.
p.addParameter('Dec',400,@isscalar);
% number of points used in the FFT during the stable/unstable projection
p.addParameter('FFTpoints',2^14,@isnatural);
% You can provide this to obtain custom HB settings. This option is very
% usefull in an oscillator analysis
p.addParameter('HBsettings',[],@isstruct);
% list of nodes that should not be used in the stability analysis
p.addParameter('excludeNodes',{},@iscellstr);
% technique used to determine the stability of the orbit
p.addParameter('StabilityAnalysisTechnique','PROJECTION',@(x) any(strcmpi(x,{'POLEZERO','PROJECTION','SKIP'})));
% number of HTFs to be included in the analysis
p.addParameter('numHTFs',3,@isnatural);
% minimum order to be estimated
p.addParameter('minOrder',1,@isnatural);
% maximum order to be estimated
p.addParameter('maxOrder',20,@isnatural);
% step used in the order sweep
p.addParameter('orderStep',1,@isnatural);
% asymptotic behaviour of the circuit at infinite frequency. This
% determines wheter a proper, strictly proper or improper model is fitted.
% if asymptotic=1, a strictly proper model is estimated. If asymptotic=2, a
% proper model is estimated, if asymptotic=3, an improper model, with an
% extra zero is estimated.
p.addParameter('asymptotic',1,@(x) any(x==[1 2 3]));

p.addParameter('FourierCoeffs',300,@isnatural)
p.parse(Netlist,MSdef,varargin{:});
args = p.Results;
extra_args = p.Unmatched;
clear p Netlist MSdef varargin

% if the netlist is a filename, read it into a cell array
if ischar(args.Netlist)
    args.Netlist = readTextFile(args.Netlist);
end

% if a linear step is provided, clear the amount of points per decade
if ~isempty(args.Step)
    args.Dec=[];
end

% is MSnode in MSdef is just one node, turn it into a cell array where the second node is connected to ground
for ii=1:length(args.MSdef)
    if ischar(args.MSdef(ii).MSnode)
        args.MSdef(ii).MSnode = {args.MSdef(ii).MSnode '0'};
    end
end

% check whether the amount of FFT points is a nice power of two
if log2(args.FFTpoints)~=round(log2(args.FFTpoints));
    args.FFTpoints = 2^ceil(log2(args.FFTpoints));
    warning(['the amount of FFTpoint specified was no power of 2, rounding up to ' num2str(args.FFTpoints) ' points instead']);
end

%% if the nodes list is empty, run a DC simulation to determine the nodes in the circuit
if isempty(args.nodes)
    L = length(args.Netlist);
    % replace the multisine source by its DC source and output resistance
    args.Netlist=replaceMS(args.Netlist,args.MSdef);
    % add a DC simulation statement to the netlist
    args.Netlist{end+1} = ADSaddDC(-1,'DCSIM');
    % run a DC simulation
    writeTextFile(args.Netlist,'temp.temp');
    DC = ADSrunSimulation('temp.temp');
    delete('temp.temp');
    DC = DC.sim1_DC;
    % remove the added statements from the netlist
    args.Netlist = args.Netlist(1:L);
    % determine the nodes by looking at the fields in the resulting DC struct
    args.nodes = fieldnames(DC);
    args.nodes = args.nodes(cellfun(@isempty ,regexp(args.nodes,'(^I_)|(^freq$)|(^MSinternal[0-9]+$)')));
end

%% run the simulations. 
% if all tones in the multisine source are zero, a DC stability analysis is
% performed with AC analyses. If there are nonzero tones in the multisine,
% an LSSS is performed. The results will be gathered in a struct SIM and
% the FRFs for stability analysis will be gathered in a matrix Z
Z = [];
leg = {};
if all(args.MSdef.ampl==0)&&isempty(args.HBsettings);
    %% AC simulations case
    % replace the multisine source by its DC source and output resistance
    args.Netlist=replaceMS(args.Netlist,args.MSdef);
    % create the MSdef sources that represent the tickler tones
    MSdef = MScreate(1,1,'Rout',Inf);
    % the multisine sources describe the location of the ticklers
    for nn=1:length(args.nodes)
        if nn>1
            MSdef(nn) = MSdef(1);
        end
        MSdef(nn).MSnode = {args.nodes{nn} ,'0'};
    end
    % run the simulation
    switch args.StabilityAnalysisTechnique
        case 'PROJECTION'
            % If the projection-based analysis is used, simulate on the
            % pre-compensated frequency grid, to avoid interpolation
            SIM = ADSsimulateAC(args.Netlist,MSdef,...
                'Stop',args.Stop,'DiskGrid',true,'DiskGridPoints',log2(args.FFTpoints));
        case 'POLEZERO'
            SIM = ADSsimulateAC(args.Netlist,MSdef,...
                'Start',args.Start,'Stop',args.Stop,'Step',args.Step,'Dec',args.Dec);
    end
    SPEC=[];
    % remove some unneeded nodes from the simulation results.
    SIM = cleanupSIM(SIM,args.excludeNodes);
    % get the voltages in the simulation results
    V = fieldnames(SIM);
    V = V(cellfun(@isempty ,regexp(V,'(^I_)|(^freq$)|(^MSinternal[0-9]+$)')));
    % gather the voltages in a big Z-matrix
    for ss=1:length(args.nodes)
        for vv=1:length(V)
            Z(end+1,:) = SIM.(V{vv})(ss,:);
            leg{end+1} = sprintf('I_{%s} -> V_{%s}',args.nodes{ss},V{vv});
        end
    end
    freq = SIM.freq;
else
    %% LSSS simulation case
    % this case is easier, because most of the implementation is already
    % done in ADSsimulateLSSS. Some post-processing is required though
    
    % run the simulation
    if ~isempty(args.HBsettings)
        [SIM,SPEC] = ADSsimulateLSSS(args.Netlist,args.MSdef,'LSSSNode',args.nodes,...
            'Start',args.Start,'Stop',args.Stop,'Step',args.Step,'Dec',args.Dec,'SimSettingsStruct',args.HBsettings,extra_args);
    else
        [SIM,SPEC] = ADSsimulateLSSS(args.Netlist,args.MSdef,'LSSSNode',args.nodes,...
            'Start',args.Start,'Stop',args.Stop,'Step',args.Step,'Dec',args.Dec,extra_args);
    end
    freq = SIM.ssfreq;
    % extract the system frequency from the HB result
    SIM.fsys = SPEC.freq(2);
    % remove some unneeded nodes from the simulation results.
    SIM = cleanupSIM(SIM,args.excludeNodes);
    % do the bookkeeping on the LSSS result to obtain the HTFs. I use a
    % cosine base, to obtain nicely behaving frequency response functions
    SIM = LSSSextractHTFs(SIM,'cos');
    % get the voltages in the simulation results
    V = fieldnames(SIM);
    V = V(cellfun(@isempty ,regexp(V,'(^I_)|(^freq$)')));
    % HTF0 contains the index of the zeroeth HTF in the list
    HTF0ind = (size(SIM.(V{1}),2)+1)/2;
    % gather the voltages in a big Z-matrix
    for ss=1:length(args.nodes)
        for vv=1:length(V)
            for hh=-args.numHTFs:args.numHTFs
                Z(end+1,:) = SIM.(V{vv})(ss,HTF0ind+hh,:);
                leg{end+1} = sprintf('I_{%s} -> V_{%s} (%s)',args.nodes{ss},V{vv},num2str(hh));
            end
        end
    end
    
end


%% TODO: now perform the actual stability analysis
switch upper(args.StabilityAnalysisTechnique)
    case 'POLEZERO'
        % in this technique, we apply vector fitting to the results to estimate
        % the poles and zeroes of the FRFs. If there are poles in the right
        % half plane, the orbit is unstable
        disp('Estimating the poles and zeroes of the system');
        
        % generate the vector of orders
        StabRes.Order = args.minOrder:args.orderStep:args.maxOrder;
        
        % preallocate some of the results
        StabRes.SER    = cell( 1,length(StabRes.Order));
        StabRes.poles  = cell( 1,length(StabRes.Order));
        StabRes.Zfit   = cell( 1,length(StabRes.Order));
        StabRes.rmserr = zeros(1,length(StabRes.Order));
        
        % remove perfect zeros from Z
        Z(Z==0)=eps;
        
        % sweep the order while estimating, to find the optimal one
        for nn=1:length(StabRes.Order);
            order = StabRes.Order(nn);
            fprintf('ORDER=%d\n',order);
            [StabRes.SER{nn},StabRes.poles{nn},StabRes.rmserr(nn),StabRes.Zfit{nn}] = ...
                VF(Z,2*pi*1i*freq,[],'stable',false,'Niter1',200,'Niter2',5,'asymp',args.asymptotic,'plot',false,...
                'N',order,'weightparam',2);
        end
        
        % if we are estimating on the HTFs, pole clustering should be
        % applied to get the poles on vertical lines on the imaginary plane
        % TODO
        
        % plot the fitting error and color it according to stable and unstable
        stab = cellfun(@(x)~any(real(x)>0),StabRes.poles);
        figure(2)
        plot(StabRes.Order((stab)),log10(StabRes.rmserr(stab)),'o','Color',[0 150/255 0]);
        hold on
        plot(StabRes.Order((~stab)),log10(StabRes.rmserr(~stab)),'rx');
        plot(StabRes.Order,log10(StabRes.rmserr),'-','Color',[150/255 150/255 150/255]);
        xlabel('order')
        ylabel('log_{10}(rms error)')
        
        
    case 'PROJECTION'
        % call the StableUnstableProjection function
        tic
        [StabRes.ystable,StabRes.yunstable,StabRes.fs,StabRes.fus,StabRes.poles,StabRes.freq,StabRes.Z_filtered] = ...
            StableUnstableProjection(Z,freq,'resample',false,'numberOfCoefficients',args.FourierCoeffs,'Filtertype','LOWPASS');
        disp(['The stability analysis took ' num2str(toc) 's'])
    case 'SKIP'
        StabRes = [];
end

end

function netlist=replaceMS(netlist,MSdef)
% this function adds the small-signal equivalent of the multisine sources
% to a netlist. The multisines are replaces by a DC voltage source with a
% series resistor. If the multisine is a current source, the source is not
% replaced.
for ii=1:length(MSdef)
    if ~isinf(MSdef(ii).Rout)
    % replace the multisine by a DC voltage source and its resistor
    netlist{end+1} = ADSaddV_Source(-1,sprintf('MS_DCsource%d',ii),...
        'p',sprintf('MSinternal%d',ii),'n',MSdef(ii).MSnode{2},'Vdc',MSdef(ii).DC,'Vac',0);
    netlist{end+1} = ADSaddR(-1,sprintf('MS_outputRes%d',ii),...
        'p',sprintf('MSinternal%d',ii),'n',MSdef(ii).MSnode{1},'R',MSdef(ii).Rout);
    end
end
end

function SIM = cleanupSIM(SIM,excludenodes)
% this function cleans up the simulation results by removind unneeded nodes
% from the simulation struct.
    for ii=1:length(excludenodes)
        % I do this in a for loop with a try, to avoid a crash is the node is not in the struct
        try
            SIM = rmfield(SIM,excludenodes{ii});
        catch
            warning('Tried to remode results from node %s, but that node was not in the simulation results',excludenodes{ii});
        end
    end
end

% @generateFunctionHelp

% @Tagline checks the local stability of a solution of a circuit.

% @Outputs{1}.Description{1} This struct contains the results of the stability analysis. If
% @Outputs{1}.Description{2} pole-zero fitting is used, it contains the results of the vector fitting
% @Outputs{1}.Description{3} routines for different orders.
% @Outputs{1}.Type struct

% @Outputs{2}.Description{1} NxF matrix of impedance functions found in the linearisation of
% @Outputs{2}.Description{2} the circuit. If a large-signal orbit is considered, the HTFs are also
% @Outputs{2}.Description{3} added to Z
% @Outputs{2}.Type matrix

% @Outputs{3}.Description frequency vector of the found impedances
% @Outputs{4}.Type vector

% @Outputs{4}.Description legend of the impedances
% @Outputs{4}.Type cell array

% @Outputs{5}.Description raw simulation results from ADS
% @Outputs{5}.Type struct

% @Outputs{6}.Type struct
% @Outputs{6}.Description Spectrum of the Large-signal solution