function AC = ADSsimulateAC(netlist,MSdef,varargin)
% ADSSIMULATEAC performs an AC simulation in ADS
%
%    AC =  ADSSIMULATEAC(netlist,MSdef)
%    AC =  ADSSIMULATEAC(netlist,MSdef,'ParamName',ParamValue)
%
% 
%
% Required Inputs:
%   netlist  Default:  CheckFunction: @(x) ischar(x)|iscellstr(x)
%     filename of the netlist that will be simulated.
%   MSdef  Default:  CheckFunction: @isstruct
%     Describes the Multisines in the netlist. The multisine is used to
%     determine the location of the AC source in the circuit and its output
%     impedance. Use MScreate to design a multisine. If you don't provide a
%     frequency range to the function, the frequency range is also extracted
%     from the multisine.
% Parameter-Value pairs:
%   Start  Default: [] CheckFunction: @isscalar
%     start frequency of the AC sweep. If you don't specify this, the in-band
%     range of the multisine is used
%   Stop  Default: 1 CheckFunction: @isscalar
%     stop frequency of the AC sweep
%   Step  Default: 1 CheckFunction: @(x) isscalar(x)||isempty(x)
%     frequency step of the AC sweep
%   Dec  Default: [] CheckFunction: @(x) isscalar(x)||isempty(x)
%     Points per decade in the AC sweep. if this parameter is set, a
%     logarithmic sweep is performed
%   DiskGrid  Default: false CheckFunction: @islogical
%     if this is set to true, a special frequency grid will be used that, when
%     transformed onto the unit disk, will yield a linearly spaced theta
%     vector. This option is used in the projection-based stability analysis
%   DiskGridPoints  Default: 14 CheckFunction: @isnatural
%     This controls the power of two that determines the amount of points used
%     in the fft for the special grid. if this parameter is set to N, a 2^N
%     point fft is used.
%   cleanup  Default: true CheckFunction: @islogical
%     if this is set to false, the temporary files are not deleted
% 
% Outputs: 
%   AC Type: struct
%     contains the result of the AC simulation. For each current and
%     voltage in the circuit there will be a field here with the same name and
%     a vector with results. Also a freq field is added that contains the
%     frequency axis
%
% 
% See Also: 


p = inputParser();
p.KeepUnmatched = true;
p.FunctionName= 'ADSsimulateAC';
% filename of the netlist that will be simulated.
p.addRequired('netlist',@(x) ischar(x)|iscellstr(x));
% Describes the Multisines in the netlist. The multisine is used to
% determine the location of the AC source in the circuit and its output
% impedance. Use MScreate to design a multisine. If you don't provide a
% frequency range to the function, the frequency range is also extracted
% from the multisine.
p.addRequired('MSdef',@isstruct);
% start frequency of the AC sweep. If you don't specify this, the in-band
% range of the multisine is used
p.addParamValue('Start',[],@isscalar);
% stop frequency of the AC sweep
p.addParamValue('Stop',1,@isscalar);
% frequency step of the AC sweep
p.addParamValue('Step',1,@(x) isscalar(x)||isempty(x));
% Points per decade in the AC sweep. if this parameter is set, a
% logarithmic sweep is performed 
p.addParamValue('Dec',[],@(x) isscalar(x)||isempty(x));
% if this is set to true, a special frequency grid will be used that, when
% transformed onto the unit disk, will yield a linearly spaced theta
% vector. This option is used in the projection-based stability analysis
p.addParamValue('DiskGrid',false,@islogical);
% This controls the power of two that determines the amount of points used
% in the fft for the special grid. if this parameter is set to N, a 2^N
% point fft is used.
p.addParamValue('DiskGridPoints',14,@isnatural);
% if this is set to false, the temporary files are not deleted
p.addParamValue('cleanup',true,@islogical);
p.parse(netlist,MSdef,varargin{:});
args = p.Results();
clear netlist MSdef varargin p

numMS = length(args.MSdef);

% if the netlist is a string it is a filename, read in its content
if ischar(args.netlist)
    args.netlist = readTextFile(args.netlist);
end

% if the MSnode is a char, turn it into a cell array
for ii=1:numMS
    if ischar(args.MSdef(ii).MSnode)
        args.MSdef(ii).MSnode = {args.MSdef(ii).MSnode '0'};
    end
end

% if no frequency range is provided, use the frequency span of the multisine
if ~args.DiskGrid
    if isempty(args.Start)
        args.Start = args.MSdef(1).fmin;
        args.Stop = args.MSdef(1).fmax;
        args.Step = args.MSdef(1).f0;
    end
end


% Add the variables for the sweep over the different ticklers
if args.DiskGrid
    % if the special DiskGrid is used, the frequency sweep will be
    % performed outside of the AC simulation, so we sweep the FreqSweep,
    % instead of the ACsim itself
    args.netlist{end+1} = ADSaddParamSweep(-1,'TickleSweep','SimInstanceName','FreqSweep','SweepVar','SS_ampindex','SweepPlan','TickleSweepPlan');
else
    args.netlist{end+1} = ADSaddParamSweep(-1,'TickleSweep','SimInstanceName','ACsim','SweepVar','SS_ampindex','SweepPlan','TickleSweepPlan');
end

args.netlist{end+1} = ADSaddSweepPlan(-1,'TickleSweepPlan','Start',1,'Stop',numMS,'Step',1);
args.netlist{end+1} = sprintf('SS_ampindex=1');
args.netlist{end+1} = sprintf('SS_amplitude=%s',generateAmpList(numMS));
for ii=1:numMS
    if isinf(args.MSdef(ii).Rout)
        args.netlist{end+1} = ADSaddI_Source(-1,['ACsource' num2str(ii)],'source',args.MSdef(ii).MSnode{1},'sink',args.MSdef(ii).MSnode{2},'Idc',args.MSdef(ii).DC,'Iac',sprintf('SS_amplitude[%d*(int(SS_ampindex)-1)+%d]',numMS,ii));
    else
        args.netlist{end+1} = ADSaddV_Source(-1,['ACsource' num2str(ii)],'p',args.MSdef(ii).MSnode{1},'n',args.MSdef(ii).MSnode{2},'Vdc',args.MSdef(ii).DC,'Vac',sprintf('SS_amplitude[%d*(int(SS_ampindex)-1)+%d]',numMS,ii));
    end
end

if args.DiskGrid
    % SS_N is the power of two that indicates the amount of points on the unit circle.
    args.netlist{end+1} = sprintf('SS_N=%s',num2str(args.DiskGridPoints));
    % SS_Fmax is the maximum frequency in the simulation
    args.netlist{end+1} = sprintf('SS_Fmax=%s',eng(args.Stop));
    % SS_Norm is the normalisation used after theta is translated to theimaginary axis
    args.netlist{end+1} = 'SS_Norm=SS_Fmax/(1+sqrt(2))';
    % SS_ind is the index that will be swept
    args.netlist{end+1} = 'SS_ind=1';
    % SS_theta are the wanted angles on the circle. They go from pi to -pi/4
    args.netlist{end+1} = 'SS_theta=2*pi*SS_ind/(2^SS_N)';
    % SS_Z is the complex number related to the corresponding theta
    args.netlist{end+1} = 'SS_Z=exp(j*SS_theta)';
    % SS_Freq is the frequency at which the simulation will be performed
    args.netlist{end+1} = 'SS_Freq=imag((SS_Z+1)/(SS_Z-1))*SS_Norm';
    % The AC simulation is run on a single frequency SS_Freq, which will be swept manually
    args.netlist{end+1} = ADSaddAC(-1,'ACsim','Freq','SS_Freq');
    % The custom frequency sweep changes the index
    args.netlist{end+1} = 'ParamSweep:FreqSweep SimInstanceName[1]="ACsim" SweepVar="SS_ind" SweepPlan="FreqSweepPlan"';
    % the index is swept over values that correspond to the pi -> -pi/4 interval
    args.netlist{end+1} = 'SweepPlan:FreqSweepPlan Start=(2^SS_N/2) Stop=(2^SS_N-2^SS_N/8) Step=1';
else
    % add the AC simulation statement
    args.netlist{end+1} = ADSaddAC(-1,'ACsim','SweepVar','freq','SweepPlan','ACsweep');
    
    % add the frequency sweep plan
    if isempty(args.Dec)
        % linear sweep
        args.netlist{end+1} = ADSaddSweepPlan(-1,'ACsweep','Start',eng(args.Start,3,10),'Stop',eng(args.Stop,3,10),'Step',eng(args.Step,3,10));
    else
        % logarithmic sweep
        args.netlist{end+1} = ADSaddSweepPlan(-1,'ACsweep','Start',eng(args.Start,3,10),'Stop',eng(args.Stop,3,10),'Dec',args.Dec);
    end
end

% run the simulation
writeTextFile(args.netlist,'temp.net');
AC = ADSrunSimulation('temp.net');


% now get the simulation results out of the struct
AC = AC.sim1_AC;

% remove the SS_ampindex sweep variable
AC = rmfield(AC,'sweepvar_SS_ampindex');


if args.DiskGrid
    % if the DiskGrid parameter is true, the frequency axis generated by ADS is
    % rubbish, regenerate it by calculating the same formulas as those that
    % were passed to the netlist earlier
    SS_N=args.DiskGridPoints;
    SS_Fmax=args.Stop;
    SS_Norm=SS_Fmax/(1+sqrt(2));
    SS_theta=2*pi*AC.sweepvar_SS_ind/(2^SS_N);
    SS_Z=exp(1i*SS_theta);
    AC.freq = imag((SS_Z+1)./(SS_Z-1))*SS_Norm;
    % remove the SS_ind field
    AC = rmfield(AC,'sweepvar_SS_ind');
    % Also, due to the peculiar sweep setup, the fields in the resulting
    % struct have to be transposed
    fields = fieldnames(AC);
    for ss=1:length(fields)
        AC.(fields{ss}) = AC.(fields{ss}).';
    end
end

% if needed, clean up the temporary netlist
if args.cleanup
    delete('temp.net')
end

end

%% this function generates an 
function res=generateAmpList(n)
% the excitation matrix is a diagonal matrix
mat = eye(n);
% start the list statement
res = 'list(';
% add the ones and zeros of the excitation matrix
for ii=1:n
    for jj=1:n
        res = [res num2str(mat(ii,jj)) ','];%#ok
    end
end
% there's a comma too much, throw it away
res = res(1:end-1);
% add the closing bracket
res = [res ')'];
end


% @generateFunctionHelp


% @Tagline performs an AC simulation in ADS

% @Outputs{1}.Description{1} contains the result of the AC simulation. For each current and
% @Outputs{1}.Description{2} voltage in the circuit there will be a field here with the same name and
% @Outputs{1}.Description{3} a vector with results. Also a freq field is added that contains the
% @Outputs{1}.Description{4} frequency axis
% @Outputs{1}.Type struct