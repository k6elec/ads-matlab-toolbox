function res = Num2ADScomplexStr(num)
if real(num)~=0
    r = num2str(real(num));
    if imag(num)~=0
        i=num2str(imag(num));
        res = [r '+' i '*j'];
    else
        res = r;
    end
else
    if imag(num)~=0
        i=num2str(imag(num));
        res = [ i '*j'];
    else
        res = '0';
    end
end
end