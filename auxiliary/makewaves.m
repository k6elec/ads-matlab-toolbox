function [A,B]=makewaves(V,I,Z0,type)
% this function makes wave spectra out of current and voltage spectra.
%
%   [A,B] = makewaves( V , I , Z0 )
%   [A,B] = makewaves( V , I , Z0 , type)
%
% With:
%   V   Is the spectrum of the measured voltage at the node. 
%   I   Is the spectrum of the measured current at the node.
%
%       Both V and I should have the same size. 
%
%   Z0  Is the characteristic impedance used to construct the waves. It should be a scalar.
%
%   type type of waves that is wanted, current options are 
%           'power'     power waves of Kurokawa
%           'pseudo'    pseudo waves of Marks and Williams
%
%   For real characteristic impedances, both wave representations are
%   equivalent.
%
% Output:
%   A   a matrix which contains the incoming waves
%   B   a matrix which contains the outgoing waves
%       both A and B have the same size as V and I
%
%   ELEC VUB; Adam Cooman

if ~exist('type','var')
    type='pseudo';
end

if ~isscalar(Z0)
    error('Z0 should be a scalar.')
end

% Z0 is an N x N diagonal matrix which contains the square root of the characteristic impedance
%   V is a matrix with the same size as I
switch lower(type)
    case 'power'
        % power waves of kurokawa
        fac = 0.5/real(Z0);
        A = fac * ( V + Z0 * I );
        B = fac * ( V - Z0 * I );
    case 'pseudo'
        % pseudo waves of Marks and Williams
        fac = 0.5*sqrt(real(Z0))/abs(Z0);
        A = fac * ( V + Z0 * I );
        B = fac * ( V - Z0 * I );
    otherwise
        error('wave type is unknown')
end

end