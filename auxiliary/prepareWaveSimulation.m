function [Netlist,probeNames] = prepareWaveSimulation(Netlist)
% prepareWaveSimulation renames the second node of current probes named I_XXXX into V_XXXX. It also makes sure the rest of the netlist is updated
%
%   Netlist = prepareWaveSimulation(Netlist)
%
% Adam Cooman, ELEC VUB


% temporarily take sub-circuits out of the netlist, the replacement should not occur within them
subcircuits = regexp(Netlist,'^define\s+(?<name>[a-zA-Z][a-zA-Z0-9_]+)\s+\(.*?\)\s*$','names');
subcircstarts = find(cellfun(@(x) ~isempty(x),subcircuits));
subcircuits = subcircuits(subcircstarts);
inds = false(1,length(Netlist));
for ii=1:length(subcircuits)
    % go look for the end of the subcircuit definition
    subcircends = find(cellfun(@(x) ~isempty(x), regexp(Netlist,['^end\s+' subcircuits{ii}.name '\s*$'],'once')));
    inds(subcircstarts:subcircends)=true;
end
Subcircuits = Netlist(inds);
Netlist = Netlist(~inds);


% find the current source statements
CurrentSourceStatements = Netlist(cellfun(@(x) ~isempty(x),regexp(Netlist,'^\s*Short:\s*I_')));

% check the nodes of the current sources
probes = regexp(CurrentSourceStatements,'^\s*Short:\s*I_(?<name>[a-zA-Z0-9_]+)+\s+(?<p1>[a-zA-Z_0-9]+)\s+(?<p2>[a-zA-Z0-9_]+)\s','names');

% replace the found nodes in the netlist by V_XXX_pY
for ii=1:length(probes)
    Netlist = regexprep(Netlist,['\s+' probes{ii}.p2 '\s+'],[' V_' probes{ii}.name ' ']);
end

% add the subcircuits back to the netlist
Netlist = [Netlist;Subcircuits];

probeNames = [probes{:}];
probeNames = {probeNames.name};

end