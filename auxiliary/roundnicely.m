function k=roundnicely(in)
% ensures that 1/x can be written down with a finite number of digits
%
%   k = roundnicely( x )
%
% if x is 3, k is 4, such that 1/k is 0.25 instead of 0.333333...
% if x is 700, k is 800, such that 1/k is 0.00125
%
% Adam Cooman, ELEC VUB

% I write down the number in engineering notation X.XXXXXe+Y and pass that
% through regexp to obtain the number X.XXXXX and the power P
t=regexp(sprintf('%e',in),'(?<significant>[0-9]\.[0-9]+)e[+-](?<power>[0-9]+)','names');
% I just work on the X.XXXXX part, since the power always rounds nicely.
kt=str2double(t.significant);
% I look at the significant part and round it up to an appropriate value
if kt<2
    k = 2*10^(str2double(t.power));
elseif kt<4
    k = 4*10^(str2double(t.power));
elseif kt<5
    k = 5*10^(str2double(t.power));
elseif kt<8
    k = 8*10^(str2double(t.power));
else
    k = 10*10^(str2double(t.power));
end
end