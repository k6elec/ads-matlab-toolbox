function res = makeallwaves(simres,Z0,type,newstruct)
% makeallwaves takes the V_XXX and I_XXX fields in a struct and combines them into waves
%
%   res = makeallwaves(simres)
%   res = makeallwaves(simres,Z0,type,newstruct)
%
% Z0 is the reference impedance. Default=50
% type is the type of wave (pseudo or power). Default=pseudo
% if newstruct is set to true, the result variable will only contain the
% waves and a frequency axis (Default=true)
%
% Adam Cooman, ELEC VUB

switch nargin
    case 1
        Z0 = 50;
        type = 'pseudo';
        newstruct = true;
    case 2
        type = 'pseudo';
        newstruct = true;
    case 3
        newstruct = true;
    case 4
end
if ~isscalar(Z0); error('Z0 should be scalar'); end;
if ~any(strcmp(type,{'pseudo','power'})); error('wave types should be power or pseudo'); end;
if ~islogical(newstruct); error('newstruct input should be logical'); end;

% if no new struct is wanted, fill the result struct with the fields of the original struct
if ~newstruct
    res = simres;
else
    res = struct();
end

% get the fieldnames of the input struct
fields = fieldnames(simres);
% find the current fields in the simres struct
currents = regexp(fields,'^I_(?<name>.+)$','names');
currents = currents(cellfun(@(x) ~isempty(x),currents));
currents = [currents{:}];
currents = {currents.name};

% build the waves
for ii=1:length(currents)
    V_field = ['V_' currents{ii}];
    I_field = ['I_' currents{ii}];
    if isfield(simres,V_field);
        [res.(['W_' currents{ii} '_A']),res.(['W_' currents{ii} '_B'])]=makewaves(simres.(V_field),simres.(I_field),Z0,type);
    end
end

% if possible, add the frequency axis to the new struct
if newstruct
    if isfield(simres,'freq')
        res.freq = simres.freq;
    end
end