classdef inputParserEgon < inputParser
    properties
        ResultsMassaged = struct();
    end
    methods
        function obj = parse(obj, varargin)
            % validate
            parse@inputParser(obj,varargin{:});
            % normalize            
            WARNMSG = 'MATLAB:structOnObject';
            oldWarn = warning('off',WARNMSG);
            data = struct(obj);
            for s = [data.Required data.Optional data.ParamValue]
                currentValue = obj.Results.(s.name);
                if hasValidatorAndNormalizer(s)
                    [obj.ResultsMassaged.(s.name)] = normalize(s, currentValue);
                else
                    [obj.ResultsMassaged.(s.name)] = currentValue;
                end
            end
            warning(oldWarn.state, WARNMSG);
            function bool = hasValidatorAndNormalizer(s)
                hasTwo = @(x) (x >= 2);
                valFunc = s.validator;
                bool = ~isempty(valFunc) && ...
                       hasTwo(nargout(valFunc));
            end
            function val = normalize(s, val)
                [~, val] = feval(s.validator, val);
            end
        end
    end
end