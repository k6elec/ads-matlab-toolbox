function [tf,x] = isNetlist(x)
if ischar(x)
    x = readTextFile(x);
    tf=true;
elseif iscellstr(x)
    tf=true;
else
    tf=false;
end
end