function res = ADSsetParameter(res,name,value)
        if ischar(value)
            res.(genvarname(name))=value;
        elseif isnumeric(value)
            if length(value)==1
                res.(name)=num2str(value);
            else
                for jj=1:length(value)
                    res.(genvarname([name '[' num1str(jj) ']']))=num2str(value(jj));
                end
            end
        end
    end
